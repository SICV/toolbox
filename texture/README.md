texture
=========

Uses tesseract (& leptonica) to detect characters in an image.

<s>&copy;</s> S I C V 2 0 1 6  
Free Art License


Usage
-------------

	texture input.png_or_jpg > output.svg


Example
-------------
![](img/hello.png) &rArr; ![](img/hello.texture.svg)


### stderr
```
image size: 354, 213
SYM:'H', conf: 98.85; BoundingBox: 94,29,116,58; psize: 29, font id: 316
SYM:'e', conf: 92.24; BoundingBox: 122,36,143,59; psize: 29, font id: 316
SYM:'l', conf: 96.88; BoundingBox: 149,27,152,58; psize: 29, font id: 316
SYM:'l', conf: 96.88; BoundingBox: 160,27,163,58; psize: 29, font id: 316
SYM:'o', conf: 95.51; BoundingBox: 169,36,189,59; psize: 29, font id: 316
SYM:'w', conf: 96.80; BoundingBox: 206,36,235,58; psize: 29, font id: 316
SYM:'o', conf: 97.23; BoundingBox: 239,36,259,59; psize: 29, font id: 316
SYM:'r', conf: 95.28; BoundingBox: 265,36,278,58; psize: 29, font id: 316
SYM:'l', conf: 97.80; BoundingBox: 281,27,285,58; psize: 29, font id: 316
SYM:'d', conf: 94.60; BoundingBox: 291,27,311,59; psize: 29, font id: 316
SYM:'!', conf: 97.34; BoundingBox: 320,29,324,58; psize: 29, font id: 316
SYM:'H', conf: 85.47; BoundingBox: 46,90,54,101; psize: 10, font id: 316
SYM:'e', conf: 79.60; BoundingBox: 57,93,64,101; psize: 10, font id: 316
SYM:'l', conf: 98.75; BoundingBox: 66,90,68,101; psize: 10, font id: 316
SYM:'l', conf: 96.38; BoundingBox: 71,90,72,101; psize: 10, font id: 316
SYM:'o', conf: 78.50; BoundingBox: 74,93,82,101; psize: 10, font id: 316
SYM:'w', conf: 86.42; BoundingBox: 88,93,99,101; psize: 10, font id: 316
SYM:'o', conf: 86.61; BoundingBox: 100,93,108,101; psize: 10, font id: 316
SYM:'r', conf: 79.27; BoundingBox: 110,93,115,101; psize: 10, font id: 316
SYM:'l', conf: 98.72; BoundingBox: 116,90,118,101; psize: 10, font id: 316
SYM:'d', conf: 77.76; BoundingBox: 120,89,127,101; psize: 10, font id: 316
SYM:'.', conf: 98.54; BoundingBox: 130,99,132,101; psize: 10, font id: 316
SYM:'H', conf: 94.72; BoundingBox: 69,138,93,169; psize: 29, font id: 316
SYM:'e', conf: 94.88; BoundingBox: 98,144,118,167; psize: 29, font id: 316
SYM:'l', conf: 95.89; BoundingBox: 123,135,128,165; psize: 29, font id: 316
SYM:'l', conf: 96.04; BoundingBox: 134,134,140,165; psize: 29, font id: 316
SYM:'o', conf: 92.86; BoundingBox: 145,141,165,164; psize: 29, font id: 316
SYM:'w', conf: 94.82; BoundingBox: 181,138,210,161; psize: 29, font id: 316
SYM:'o', conf: 92.73; BoundingBox: 214,137,235,160; psize: 29, font id: 316
SYM:'r', conf: 93.35; BoundingBox: 240,135,253,158; psize: 29, font id: 316
SYM:'l', conf: 93.54; BoundingBox: 256,127,261,157; psize: 29, font id: 316
SYM:'d', conf: 92.92; BoundingBox: 266,125,287,157; psize: 29, font id: 316
SYM:'!', conf: 87.69; BoundingBox: 294,125,300,155; psize: 29, font id: 316
```

### stdout
```svg
<svg version="1.1" baseProfile="full" width="354" height="213" xmlns="http://www.w3.org/2000/svg">
<style>
/* <![CDATA[ */
rect { fill: none; stroke: #FF00FF; stroke-width: 1px; }
text { font-size: 12; font-family: sans-serif; fill: #000000; dominant-baseline: text-after-edge }
/* ]]> */
</style>
<g class="texturesymbols">
  <g class="symbol" transform="translate(94,29)"><rect x="0" y="0" width="22" height="29"/><text x="0" y="0" style="font-size: 29px"><![CDATA[H]]></text></g>
  <g class="symbol" transform="translate(122,36)"><rect x="0" y="0" width="21" height="23"/><text x="0" y="0" style="font-size: 29px"><![CDATA[e]]></text></g>
  <g class="symbol" transform="translate(149,27)"><rect x="0" y="0" width="3" height="31"/><text x="0" y="0" style="font-size: 29px"><![CDATA[l]]></text></g>
  <g class="symbol" transform="translate(160,27)"><rect x="0" y="0" width="3" height="31"/><text x="0" y="0" style="font-size: 29px"><![CDATA[l]]></text></g>
  <g class="symbol" transform="translate(169,36)"><rect x="0" y="0" width="20" height="23"/><text x="0" y="0" style="font-size: 29px"><![CDATA[o]]></text></g>
  <g class="symbol" transform="translate(206,36)"><rect x="0" y="0" width="29" height="22"/><text x="0" y="0" style="font-size: 29px"><![CDATA[w]]></text></g>
  <g class="symbol" transform="translate(239,36)"><rect x="0" y="0" width="20" height="23"/><text x="0" y="0" style="font-size: 29px"><![CDATA[o]]></text></g>
  <g class="symbol" transform="translate(265,36)"><rect x="0" y="0" width="13" height="22"/><text x="0" y="0" style="font-size: 29px"><![CDATA[r]]></text></g>
  <g class="symbol" transform="translate(281,27)"><rect x="0" y="0" width="4" height="31"/><text x="0" y="0" style="font-size: 29px"><![CDATA[l]]></text></g>
  <g class="symbol" transform="translate(291,27)"><rect x="0" y="0" width="20" height="32"/><text x="0" y="0" style="font-size: 29px"><![CDATA[d]]></text></g>
  <g class="symbol" transform="translate(320,29)"><rect x="0" y="0" width="4" height="29"/><text x="0" y="0" style="font-size: 29px"><![CDATA[!]]></text></g>
  <g class="symbol" transform="translate(46,90)"><rect x="0" y="0" width="8" height="11"/><text x="0" y="0" style="font-size: 10px"><![CDATA[H]]></text></g>
  <g class="symbol" transform="translate(57,93)"><rect x="0" y="0" width="7" height="8"/><text x="0" y="0" style="font-size: 10px"><![CDATA[e]]></text></g>
  <g class="symbol" transform="translate(66,90)"><rect x="0" y="0" width="2" height="11"/><text x="0" y="0" style="font-size: 10px"><![CDATA[l]]></text></g>
  <g class="symbol" transform="translate(71,90)"><rect x="0" y="0" width="1" height="11"/><text x="0" y="0" style="font-size: 10px"><![CDATA[l]]></text></g>
  <g class="symbol" transform="translate(74,93)"><rect x="0" y="0" width="8" height="8"/><text x="0" y="0" style="font-size: 10px"><![CDATA[o]]></text></g>
  <g class="symbol" transform="translate(88,93)"><rect x="0" y="0" width="11" height="8"/><text x="0" y="0" style="font-size: 10px"><![CDATA[w]]></text></g>
  <g class="symbol" transform="translate(100,93)"><rect x="0" y="0" width="8" height="8"/><text x="0" y="0" style="font-size: 10px"><![CDATA[o]]></text></g>
  <g class="symbol" transform="translate(110,93)"><rect x="0" y="0" width="5" height="8"/><text x="0" y="0" style="font-size: 10px"><![CDATA[r]]></text></g>
  <g class="symbol" transform="translate(116,90)"><rect x="0" y="0" width="2" height="11"/><text x="0" y="0" style="font-size: 10px"><![CDATA[l]]></text></g>
  <g class="symbol" transform="translate(120,89)"><rect x="0" y="0" width="7" height="12"/><text x="0" y="0" style="font-size: 10px"><![CDATA[d]]></text></g>
  <g class="symbol" transform="translate(130,99)"><rect x="0" y="0" width="2" height="2"/><text x="0" y="0" style="font-size: 10px"><![CDATA[.]]></text></g>
  <g class="symbol" transform="translate(69,138)"><rect x="0" y="0" width="24" height="31"/><text x="0" y="0" style="font-size: 29px"><![CDATA[H]]></text></g>
  <g class="symbol" transform="translate(98,144)"><rect x="0" y="0" width="20" height="23"/><text x="0" y="0" style="font-size: 29px"><![CDATA[e]]></text></g>
  <g class="symbol" transform="translate(123,135)"><rect x="0" y="0" width="5" height="30"/><text x="0" y="0" style="font-size: 29px"><![CDATA[l]]></text></g>
  <g class="symbol" transform="translate(134,134)"><rect x="0" y="0" width="6" height="31"/><text x="0" y="0" style="font-size: 29px"><![CDATA[l]]></text></g>
  <g class="symbol" transform="translate(145,141)"><rect x="0" y="0" width="20" height="23"/><text x="0" y="0" style="font-size: 29px"><![CDATA[o]]></text></g>
  <g class="symbol" transform="translate(181,138)"><rect x="0" y="0" width="29" height="23"/><text x="0" y="0" style="font-size: 29px"><![CDATA[w]]></text></g>
  <g class="symbol" transform="translate(214,137)"><rect x="0" y="0" width="21" height="23"/><text x="0" y="0" style="font-size: 29px"><![CDATA[o]]></text></g>
  <g class="symbol" transform="translate(240,135)"><rect x="0" y="0" width="13" height="23"/><text x="0" y="0" style="font-size: 29px"><![CDATA[r]]></text></g>
  <g class="symbol" transform="translate(256,127)"><rect x="0" y="0" width="5" height="30"/><text x="0" y="0" style="font-size: 29px"><![CDATA[l]]></text></g>
  <g class="symbol" transform="translate(266,125)"><rect x="0" y="0" width="21" height="32"/><text x="0" y="0" style="font-size: 29px"><![CDATA[d]]></text></g>
  <g class="symbol" transform="translate(294,125)"><rect x="0" y="0" width="6" height="30"/><text x="0" y="0" style="font-size: 29px"><![CDATA[!]]></text></g>
</g>
</svg>
```

Building
------------

Install requirements

	apt-get install libtesseract-dev libleptonica-dev cmake

and depending on the language you use for detection, one of:

* tesseract-ocr-deu
* tesseract-ocr-eng
* tesseract-ocr-fra
* tesseract-ocr-nor


Build with cmake, from the project folder:

	mkdir build
	cd build
	cmake ..
	make

