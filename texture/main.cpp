/*
// Based on sample script tess.cpp - Recognize text on an image using Tesseract API and print it to the screen
*/

#include <stdio.h>
#include <tesseract/baseapi.h>
#include <tesseract/strngs.h>
#include <tesseract/resultiterator.h>
#include <leptonica/allheaders.h>
#include <iostream>
// #include "opencv2/core/core.hpp"
// #include "opencv2/features2d/features2d.hpp"
// #include "opencv2/highgui/highgui.hpp"
// #include "opencv2/imgproc/imgproc.hpp"

// using namespace cv;
using namespace std;


int main(int argc, char** argv)
{
    if (argc < 7)
    {
        std::cout << "usage: texture IMAGE TEXTURE.SVG LEXICALITY.SVG TEXTLINE.SVG PARA.SVG BLOCK.SVG" << std::endl;
        return 1;
    }

    //    char *outText;
    tesseract::TessBaseAPI *api = new tesseract::TessBaseAPI();
    // Initialize tesseract-ocr with English, without specifying tessdata path
    if (api->Init(NULL, "eng")) {
        fprintf(stderr, "Could not initialize tesseract.\n");
        exit(1);
    }

    // Open input image with leptonica library
    Pix *image = pixRead(argv[1]);
    fprintf(stderr, "image size: %d, %d\n", image->w, image->h);

    api->SetImage(image);
    api->Recognize(0);

    FILE *f;

    tesseract::ResultIterator* ri = api->GetIterator();
    tesseract::PageIteratorLevel level = tesseract::RIL_SYMBOL;

    // TEXTURE (iterating at tesseract::RIL_WORD)
    f = fopen(argv[2], "w");
    fprintf(f, "<svg version=\"1.1\" baseProfile=\"full\" width=\"%d\" height=\"%d\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:inkscape=\"http://www.inkscape.org/namespaces/inkscape\" >\n", image->w, image->h);
    fprintf(f, "<style>\n");
    fprintf(f, "/* <![CDATA[ */\n");
    fprintf(f, "g.symbol rect { fill: none; stroke: #FF00FF; stroke-widcth: 1px; }\n");
    fprintf(f, "text { font-size: 12; font-family: sans-serif; fill: #000000; dominant-baseline: text-after-edge }\n");
    fprintf(f, "/* ]]> */\n");
    fprintf(f, "</style>\n");

    int confidence_level = 0;
    int j = 0;

    fprintf(f, "<g class=\"texturesymbols\" id=\"symbols\" inkscape:label=\"symbols\" inkscape:groupmode=\"layer\">\n");
    if (ri != 0) {
        do {
            const char* word = ri->GetUTF8Text(level);
            float conf = ri->Confidence(level);
            int x1, y1, x2, y2;
            ri->BoundingBox(level, &x1, &y1, &x2, &y2);
            bool ignore;
            int psize,fid;

            ri->WordFontAttributes(&ignore,&ignore,&ignore,&ignore,&ignore,&ignore,&psize,&fid);
            fprintf(stderr, "SYM:'%s', conf: %.2f; BoundingBox: %d,%d,%d,%d; psize: %d, font id: %d\n", word, conf, x1, y1, x2, y2, psize, fid);
            if(conf>50) {
                // printf("  <rect x=\"%d\" y=\"%d\" width=\"%d\" height=\"%d\"/>\n", x1, y1, (x2-x1), (y2-y1));
                // fprintf(f, "  <g class=\"symbol\" transform=\"translate(%d,%d)\"><rect x=\"0\" y=\"0\" width=\"%d\" height=\"%d\"/><text x=\"0\" y=\"%d\" style=\"font-size: %dpx\"><![CDATA[%s]]></text></g>\n", x1, y1, (x2-x1), (y2-y1), (y2-y1), psize/2, word);
                fprintf(f, "  <g class=\"symbol\" transform=\"translate(%d,%d)\"><rect x=\"0\" y=\"0\" width=\"%d\" height=\"%d\"/><text x=\"0\" y=\"%d\" style=\"font-size: %dpx\"><![CDATA[%s]]></text></g>\n", x1, y1, (x2-x1), (y2-y1), (y2-y1), 12, word);
                // printf("  <g class=\"word\" transform=\"translate(%d,%d)\"><rect x=\"0\" y=\"0\" width=\"%d\" height=\"%d\"/><text x=\"0\" y=\"0\"><![CDATA[%s]]></text></g>\n", x1, y1, (x2-x1), (y2-y1), word);
                confidence_level+=conf;
                j++;
            }
            delete[] word;
        } while (ri->Next(level));
    }
    fprintf(f, "</g>\n");
    fprintf(f, "</svg>\n");
    fclose(f);


    // LEXICALITY
    ri = api->GetIterator();
    level = tesseract::RIL_WORD;
  
    f = fopen(argv[3], "w");
    fprintf(f, "<svg version=\"1.1\" baseProfile=\"full\" width=\"%d\" height=\"%d\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:inkscape=\"http://www.inkscape.org/namespaces/inkscape\" >\n", image->w, image->h);
    fprintf(f, "<style>\n");
    fprintf(f, "/* <![CDATA[ */\n");
    fprintf(f, "g.word rect { fill: none; stroke: #00FFFF; stroke-width: 1px; }\n");
    fprintf(f, "text { font-size: 12; font-family: sans-serif; fill: #000000; dominant-baseline: text-after-edge }\n");
    fprintf(f, "/* ]]> */\n");
    fprintf(f, "</style>\n");
    fprintf(f, "<g class=\"texturesymbols\" id=\"words\" inkscape:label=\"words\" inkscape:groupmode=\"layer\">\n");
    if (ri != 0) {
        do {
            const char* word = ri->GetUTF8Text(level);
            float conf = ri->Confidence(level);
            int x1, y1, x2, y2;
            ri->BoundingBox(level, &x1, &y1, &x2, &y2);
            bool ignore;
            int psize,fid;
            ri->WordFontAttributes(&ignore,&ignore,&ignore,&ignore,&ignore,&ignore,&psize,&fid);
            fprintf(stderr, "WORD:'%s', conf: %.2f; BoundingBox: %d,%d,%d,%d; psize: %d, font id: %d\n", word, conf, x1, y1, x2, y2, psize, fid);
            if(conf>50) {
                // printf("  <rect x=\"%d\" y=\"%d\" width=\"%d\" height=\"%d\"/>\n", x1, y1, (x2-x1), (y2-y1));
                //fprintf(f, "  <g class=\"word\" transform=\"translate(%d,%d)\"><rect x=\"0\" y=\"0\" width=\"%d\" height=\"%d\"/><text x=\"0\" y=\"%d\" style=\"font-size: %dpx\"><![CDATA[%s]]></text></g>\n", x1, y1, (x2-x1), (y2-y1), (y2-y1), psize/2, word);
                fprintf(f, "  <g class=\"word\" transform=\"translate(%d,%d)\"><rect x=\"0\" y=\"0\" width=\"%d\" height=\"%d\"/><text x=\"0\" y=\"%d\" style=\"font-size: %dpx\"><![CDATA[%s]]></text></g>\n", x1, y1, (x2-x1), (y2-y1), (y2-y1), 12, word);
                // printf("  <g class=\"word\" transform=\"translate(%d,%d)\"><rect x=\"0\" y=\"0\" width=\"%d\" height=\"%d\"/><text x=\"0\" y=\"0\"><![CDATA[%s]]></text></g>\n", x1, y1, (x2-x1), (y2-y1), word);
                confidence_level+=conf;
                j++;
            }
            delete[] word;
        } while (ri->Next(level));
    }
    fprintf(f, "</g>\n");
    fprintf(f, "</svg>\n");
    fclose(f);

    // TEXTLINE
    ri = api->GetIterator();
    level = tesseract::RIL_TEXTLINE;
  
    f = fopen(argv[4], "w");
    fprintf(f, "<svg version=\"1.1\" baseProfile=\"full\" width=\"%d\" height=\"%d\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:inkscape=\"http://www.inkscape.org/namespaces/inkscape\" >\n", image->w, image->h);
    fprintf(f, "<style>\n");
    fprintf(f, "/* <![CDATA[ */\n");
    fprintf(f, "g.textline rect { fill: none; stroke: #e49f90; stroke-width: 1px; }\n");
    fprintf(f, "text { font-size: 12; font-family: sans-serif; fill: #000000; dominant-baseline: text-after-edge }\n");
    fprintf(f, "/* ]]> */\n");
    fprintf(f, "</style>\n");
    fprintf(f, "<g class=\"textline\" id=\"textline\" inkscape:label=\"textline\" inkscape:groupmode=\"layer\">\n");
    if (ri != 0) {
        do {
            const char* word = ri->GetUTF8Text(level);
            float conf = ri->Confidence(level);
            int x1, y1, x2, y2;
            ri->BoundingBox(level, &x1, &y1, &x2, &y2);
            bool ignore;
            int psize,fid;
            ri->WordFontAttributes(&ignore,&ignore,&ignore,&ignore,&ignore,&ignore,&psize,&fid);
            fprintf(stderr, "TEXTLINE:'%s', conf: %.2f; BoundingBox: %d,%d,%d,%d; psize: %d, font id: %d\n", word, conf, x1, y1, x2, y2, psize, fid);
            if(conf>50) {
                // printf("  <rect x=\"%d\" y=\"%d\" width=\"%d\" height=\"%d\"/>\n", x1, y1, (x2-x1), (y2-y1));
                fprintf(f, "  <g class=\"textline\" transform=\"translate(%d,%d)\"><rect x=\"0\" y=\"0\" width=\"%d\" height=\"%d\"/><text x=\"0\" y=\"%d\" style=\"font-size: %dpx\"><![CDATA[%s]]></text></g>\n", x1, y1, (x2-x1), (y2-y1), (y2-y1), psize/2, word);
                // printf("  <g class=\"word\" transform=\"translate(%d,%d)\"><rect x=\"0\" y=\"0\" width=\"%d\" height=\"%d\"/><text x=\"0\" y=\"0\"><![CDATA[%s]]></text></g>\n", x1, y1, (x2-x1), (y2-y1), word);
                confidence_level+=conf;
                j++;
            }
            delete[] word;
        } while (ri->Next(level));
    }
    fprintf(f, "</g>\n");
    fprintf(f, "</svg>\n");
    fclose(f);

    // PARA
    ri = api->GetIterator();
    level = tesseract::RIL_PARA;
  
    f = fopen(argv[5], "w");
    fprintf(f, "<svg version=\"1.1\" baseProfile=\"full\" width=\"%d\" height=\"%d\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:inkscape=\"http://www.inkscape.org/namespaces/inkscape\" >\n", image->w, image->h);
    fprintf(f, "<style>\n");
    fprintf(f, "/* <![CDATA[ */\n");
    fprintf(f, "g.para rect { fill: none; stroke: #85bbe9; stroke-width: 1px; }\n");
    fprintf(f, "text { font-size: 12; font-family: sans-serif; fill: #000000; dominant-baseline: text-after-edge }\n");
    fprintf(f, "/* ]]> */\n");
    fprintf(f, "</style>\n");
    fprintf(f, "<g class=\"para\" id=\"para\" inkscape:label=\"para\" inkscape:groupmode=\"layer\">\n");
    if (ri != 0) {
        do {
            const char* word = ri->GetUTF8Text(level);
            float conf = ri->Confidence(level);
            int x1, y1, x2, y2;
            ri->BoundingBox(level, &x1, &y1, &x2, &y2);
            bool ignore;
            int psize,fid;
            ri->WordFontAttributes(&ignore,&ignore,&ignore,&ignore,&ignore,&ignore,&psize,&fid);
            fprintf(stderr, "PARA:'%s', conf: %.2f; BoundingBox: %d,%d,%d,%d; psize: %d, font id: %d\n", word, conf, x1, y1, x2, y2, psize, fid);
            if(conf>50) {
                // printf("  <rect x=\"%d\" y=\"%d\" width=\"%d\" height=\"%d\"/>\n", x1, y1, (x2-x1), (y2-y1));
                fprintf(f, "  <g class=\"para\" transform=\"translate(%d,%d)\"><rect x=\"0\" y=\"0\" width=\"%d\" height=\"%d\"/><text x=\"0\" y=\"%d\" style=\"font-size: %dpx\"><![CDATA[%s]]></text></g>\n", x1, y1, (x2-x1), (y2-y1), (y2-y1), psize/2, word);
                // printf("  <g class=\"word\" transform=\"translate(%d,%d)\"><rect x=\"0\" y=\"0\" width=\"%d\" height=\"%d\"/><text x=\"0\" y=\"0\"><![CDATA[%s]]></text></g>\n", x1, y1, (x2-x1), (y2-y1), word);
                confidence_level+=conf;
                j++;
            }
            delete[] word;
        } while (ri->Next(level));
    }
    fprintf(f, "</g>\n");
    fprintf(f, "</svg>\n");
    fclose(f);


    // BLOCK
    ri = api->GetIterator();
    level = tesseract::RIL_BLOCK;
  
    f = fopen(argv[6], "w");
    fprintf(f, "<svg version=\"1.1\" baseProfile=\"full\" width=\"%d\" height=\"%d\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:inkscape=\"http://www.inkscape.org/namespaces/inkscape\" >\n", image->w, image->h);
    fprintf(f, "<style>\n");
    fprintf(f, "/* <![CDATA[ */\n");
    fprintf(f, "g.block rect { fill: none; stroke: #bccc9f; stroke-width: 1px; }\n");
    fprintf(f, "text { font-size: 12; font-family: sans-serif; fill: #000000; dominant-baseline: text-after-edge }\n");
    fprintf(f, "/* ]]> */\n");
    fprintf(f, "</style>\n");
    fprintf(f, "<g class=\"block\" id=\"block\" inkscape:label=\"block\" inkscape:groupmode=\"layer\">\n");
    if (ri != 0) {
        do {
            const char* word = ri->GetUTF8Text(level);
            float conf = ri->Confidence(level);
            int x1, y1, x2, y2;
            ri->BoundingBox(level, &x1, &y1, &x2, &y2);
            bool ignore;
            int psize,fid;
            ri->WordFontAttributes(&ignore,&ignore,&ignore,&ignore,&ignore,&ignore,&psize,&fid);
            fprintf(stderr, "BLOCK:'%s', conf: %.2f; BoundingBox: %d,%d,%d,%d; psize: %d, font id: %d\n", word, conf, x1, y1, x2, y2, psize, fid);
            if(conf>50) {
                // printf("  <rect x=\"%d\" y=\"%d\" width=\"%d\" height=\"%d\"/>\n", x1, y1, (x2-x1), (y2-y1));
                fprintf(f, "  <g class=\"block\" transform=\"translate(%d,%d)\"><rect x=\"0\" y=\"0\" width=\"%d\" height=\"%d\"/><text x=\"0\" y=\"%d\" style=\"font-size: %dpx\"><![CDATA[%s]]></text></g>\n", x1, y1, (x2-x1), (y2-y1), (y2-y1), psize/2, word);
                // printf("  <g class=\"word\" transform=\"translate(%d,%d)\"><rect x=\"0\" y=\"0\" width=\"%d\" height=\"%d\"/><text x=\"0\" y=\"0\"><![CDATA[%s]]></text></g>\n", x1, y1, (x2-x1), (y2-y1), word);
                confidence_level+=conf;
                j++;
            }
            delete[] word;
        } while (ri->Next(level));
    }
    fprintf(f, "</g>\n");
    fprintf(f, "</svg>\n");
    fclose(f);

    api->End();
    //    delete [] outText;
    pixDestroy(&image);




    return 0;
}
