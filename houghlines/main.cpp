/*
    houghlines
*/

#include <stdio.h>
#include <iostream>
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

using namespace cv;
using namespace std;

int main(int argc, char** argv)
{
    Mat src, dst;
    vector<Vec4i> lines;

    if (argc < 2)
    {
        std::cout << "usage: houghlines INPUT.png" << std::endl;
        return -1;
    }

    src=imread(argv[1],CV_LOAD_IMAGE_GRAYSCALE);
    // printf("image size: %d, %d\n", src.size[0], src.size[1]);
    Canny(src, dst, 50, 200, 3 );
    HoughLinesP( dst, lines, 1, CV_PI/180, 80, 80, 10 );

    printf("<svg version=\"1.1\" baseProfile=\"full\" width=\"%d\" height=\"%d\" xmlns=\"http://www.w3.org/2000/svg\">\n", src.size[1], src.size[0]);
    printf("<style>\n");
    printf("/* <![CDATA[ */\n");
    printf("g.houghlines line { fill: none; stroke: #444444; stroke-width: 1px; }\n");
    printf("g.houghlines line.horizontal { stroke: #44FF44; }\n");
    printf("g.houghlines line.vertical { stroke: #FF4444; }\n");
    printf("/* ]]> */\n");
    printf("</style>\n");

    printf("<g class=\"houghlines\">\n");
    printf("  <!-- %d total lines -->\n", lines.size());
    // fprintf(stderr, "%d total lines\n", lines.size());
    // CSV headers
    fprintf(stderr, "type,angle,x1,y1,x2,y2\n");
    for( size_t i = 0; i < lines.size(); i++ )
    {
        double Angle = atan2(lines[i][3]- lines[i][1], lines[i][2]- lines[i][0]) * 180.0 / CV_PI;
        double dh = abs(Angle);
        double dv = abs(abs(Angle) - 90);
        if (dh < 5) {
            printf("    <line x1=\"%d\" y1=\"%d\" x2=\"%d\" y2=\"%d\" class=\"horizontal\" />\n", lines[i][0], lines[i][1], lines[i][2], lines[i][3]);
            fprintf(stderr, "horizontal,%0.1f,%d,%d,%d,%d\n", Angle, lines[i][0], lines[i][1], lines[i][2], lines[i][3]);
        } else if (dv < 5) {
            printf("    <line x1=\"%d\" y1=\"%d\" x2=\"%d\" y2=\"%d\" class=\"vertical\" />\n", lines[i][0], lines[i][1], lines[i][2], lines[i][3]);
            fprintf(stderr, "vertical,%0.1f,%d,%d,%d,%d\n", Angle, lines[i][0], lines[i][1], lines[i][2], lines[i][3]);
        } else {
            printf("    <line x1=\"%d\" y1=\"%d\" x2=\"%d\" y2=\"%d\" />\n", lines[i][0], lines[i][1], lines[i][2], lines[i][3]);
            fprintf(stderr, "line,%0.1f,%d,%d,%d,%d\n", Angle, lines[i][0], lines[i][1], lines[i][2], lines[i][3]);
        }
        // if(abs(Angle)>=0 && abs(Angle)<=5) {
        // could also consider: 
    }
    printf("</g>\n");
    printf("</svg>\n");
    return 0;
}
