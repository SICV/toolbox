houghlines
=========

Uses OpenCV's HoughLinesP function to detect lines using the Probabilistic Hough Line Transform.

See: [http://docs.opencv.org/2.4/doc/tutorials/imgproc/imgtrans/hough_lines/hough_lines.html](http://docs.opencv.org/2.4/doc/tutorials/imgproc/imgtrans/hough_lines/hough_lines.html)

Outputs an SVG file with detected lines as *line* elements. Lines with angle +- 5 degress to 0 or 90 degrees are classed "horizonal" and "vertical" respectively.

<s>&copy;</s> S I C V 2 0 1 6  
Free Art License


Usage
---------------

	houghlines input.png_or_jpg > output.svg


Example
------------

![](img/astersticks.png) &nbsp;&nbsp;&nbsp;&nbsp;&rArr;&nbsp;&nbsp;&nbsp;&nbsp; ![](img/astersticks.houghlines.svg)

### Output

#### stderr

	12 total lines
	V angle:87.1 line:254,46 to 263,225
	H angle:-2.9 line:35,10 to 231,0
	V angle:-90.0 line:131,123 to 131,35
	H angle:0.0 line:144,131 to 231,131
	H angle:0.0 line:35,136 to 120,136
	V angle:-90.0 line:136,230 to 136,147
	V angle:87.1 line:248,35 to 258,230
	H angle:-2.9 line:34,15 to 213,6
	H angle:2.9 line:39,249 to 235,259
	H angle:2.9 line:39,244 to 234,254
	V angle:-86.2 line:5,222 to 18,28
	V angle:-85.9 line:0,224 to 14,27


#### stdout

```svg
<svg version="1.1" baseProfile="full" width="264" height="260" xmlns="http://www.w3.org/2000/svg">
<style>
/* <![CDATA[ */
g.houghlines line { fill: none; stroke: #444444; stroke-width: 1px; }
g.houghlines line.horizontal { stroke: #44FF44; }
g.houghlines line.vertical { stroke: #FF4444; }
/* ]]> */
</style>
<g class="houghlines">
  <!-- 12 total lines -->
    <line x1="254" y1="46" x2="263" y2="225" class="vertical" />
    <line x1="35" y1="10" x2="231" y2="0" class="horizontal" />
    <line x1="131" y1="123" x2="131" y2="35" class="vertical" />
    <line x1="144" y1="131" x2="231" y2="131" class="horizontal" />
    <line x1="35" y1="136" x2="120" y2="136" class="horizontal" />
    <line x1="136" y1="230" x2="136" y2="147" class="vertical" />
    <line x1="248" y1="35" x2="258" y2="230" class="vertical" />
    <line x1="34" y1="15" x2="213" y2="6" class="horizontal" />
    <line x1="39" y1="249" x2="235" y2="259" class="horizontal" />
    <line x1="39" y1="244" x2="234" y2="254" class="horizontal" />
    <line x1="5" y1="222" x2="18" y2="28" class="vertical" />
    <line x1="0" y1="224" x2="14" y2="27" class="vertical" />
</g>
</svg>
```



Building
--------------

Install requirements

	apt-get install cmake libopencv-dev


Build with cmake, from the project folder:

	mkdir build
	cd build
	cmake ..
	make

