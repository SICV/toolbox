#!/usr/bin/env python
from __future__ import print_function
import numpy as np
import cv2, os, json, sys
from argparse import ArgumentParser

p = ArgumentParser("")
p.add_argument("input")
p.add_argument("--cascades", default="./haarcascades", help="path of cascades")
p.add_argument("--frames", type=int, default=0, help="number of frames to process, default: 0 (all)")
p.add_argument("--skip", type=int, default=None, help="skip frames")
# p.add_argument("--camera", type=int, default=0, help="camera number, default: 0")
args = p.parse_args()

tpath = os.path.expanduser(args.cascades)
face_cascade = cv2.CascadeClassifier(os.path.join(tpath, 'haarcascade_frontalface_default.xml'))
eye_cascade = cv2.CascadeClassifier(os.path.join(tpath, 'haarcascade_eye.xml'))

def process_image (img):
    ret = False
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # faces = face_cascade.detectMultiScale(gray, 1.3, 5)
    faces = face_cascade.detectMultiScale(gray)
    ar_faces=[]
    ar_eyes=[]
    for (x,y,w,h) in faces:
        #print "face", (x, y, w, h)
        ar_faces.append([int(x), int(y), int(w), int(h)])
        #print type(x)
        ret = True
        cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
        #print img
        roi_gray = gray[y:y+h, x:x+w]
        roi_color = img[y:y+h, x:x+w]
        eyes = eye_cascade.detectMultiScale(roi_gray)
        for (ex,ey,ew,eh) in eyes:
            cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(0,255,0),2)
            aex=int(ex)+int(x)
            aey=int(ey)+int(y)
            ar_eyes.append([aex,aey,int(ew),int(eh)]);
            #print 'eye'
    return ret, ar_faces, ar_eyes

cap = cv2.VideoCapture(args.input)
frameno = 0
# dt = datetime.datetime.now()
out = {}
ff = out["frames"] = []

while(True):
    # Capture frame-by-frame
    sys.stderr.write("\r{0}...".format(frameno))
    sys.stderr.flush()
    ret, frame = cap.read()
    if args.skip and frameno < args.skip:
        frameno += 1
        continue
    d, faces, eyes = process_image(frame)
    if d:
        print (frameno, faces, file=sys.stderr)
        data = {"frameno": frameno, "faces": faces, "eyes": eyes}
        ff.append(data)
        print (json.dumps(data))
        sys.stdout.flush()

    # tstamp = dt.strftime("%Y%m%d_%H%M%S")
    # outpath = "frame{0}_{1:04d}.jpg".format(tstamp, frameno)
    # outpath = os.path.join(args.path, outpath)
    # cv2.imwrite(outpath, frame)
    # sys.stdout.write(outpath+"\n")
    # sys.stdout.flush()
    frameno += 1
    if args.frames and frameno >= args.frames:
        break
cap.release()

print ("\n---\n")
print (json.dumps(out))

