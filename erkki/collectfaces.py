from argparse import ArgumentParser
import sys, cv2, json

p = ArgumentParser("")
p.add_argument("input")
p.add_argument("--output", default="output.avi")
p.add_argument("--fourcc", default="XVID", help="MJPG,mp4v,XVID")
args = p.parse_args()

fdata = []
for line in sys.stdin:
    line = line.strip()
    data = json.loads(line)
    fdata.append(data)
    if line.startswith("---"):
        break

finalframe = fdata[-1]["frameno"]

cap = cv2.VideoCapture(args.input)
num_frames = int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_COUNT))
width = int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_WIDTH))
height = int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT))

print ((width, height), num_frames)

# try:
fourcc = cv2.cv.CV_FOURCC(*args.fourcc)
#fourcc = cv2.cv.CV_FOURCC(*'MJPG')
# else:
#     fourcc = cv2.VideoWriter_fourcc(*'XVID')
out = cv2.VideoWriter()
out.open(args.output,fourcc, 25, (width, height))

frameno = 0

while(cap.isOpened()):
    sys.stderr.write("\r{0}...".format(frameno))
    sys.stderr.flush()
    ret, frame = cap.read()
    if fdata and fdata[0]["frameno"] == frameno:
        print ("output frame", frameno)
        data = fdata[0]
        fdata = fdata[1:]
        for x,y,w,h in data["faces"]:
            cv2.rectangle(frame,(x,y),(x+w,y+h),(0,0,255),3)
        for x,y,w,h in data["eyes"]:
            cv2.rectangle(frame,(x,y),(x+w,y+h),(0,255,0),3)
        out.write(frame)
    frameno += 1
    if frameno >= finalframe:
        break

out.release()
cap.release()
