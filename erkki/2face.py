from argparse import ArgumentParser
import sys, cv2, json
from numpy import zeros

p = ArgumentParser()
p.add_argument("src1")
p.add_argument("src2")
p.add_argument("--shift", type=int, default=0)
p.add_argument("--output", default="output.avi")
p.add_argument("--framerate", type=float, default=25, help="output frame rate")
p.add_argument("--frames", type=int, default=None, help="output only so many frames")
p.add_argument("--data1", default="faces.json", help="json face data for src1")
p.add_argument("--data2", default="faces.json", help="json face data for src1")
p.add_argument("--fourcc", default="XVID", help="MJPG,mp4v,XVID")
args = p.parse_args()

try:
    fourcc = cv2.cv.CV_FOURCC(*args.fourcc)
except AttributeError:
    fourcc = cv2.VideoWriter_fourcc(*args.fourcc)

from source import Source

src1 = Source(args.src1, args.data1)
src2 = Source(args.src2, args.data2)

src1.unparse()
src2.unparse()

out = cv2.VideoWriter()
out.open(args.output, fourcc, args.framerate, (src1.width, src1.height))
count = 0

newfaces = src2.faces(with_frame=True)
for i in range(args.shift):
     frameno, frame, face = newfaces.next()

for frameno, frame, faces, eyes in src1.frames(only_with_features=True):
    print "output frame {0}".format(count+1)

    for face in faces:
        x, y, w, h = face
        
        _, newframe, newface = newfaces.next()
        nx, ny, nw, nh = newface

        # resize newface to fit oldface
        newfaceimg = newframe[ny:ny+nh, nx:nx+nw]
        newfaceimg = cv2.resize(newfaceimg, (w, h))

        frame[y:y+h, x:x+w] = newfaceimg[0:h, 0:w]

    out.write(frame)
    count += 1
    if args.frames and count >= args.frames:
        break

out.release()


