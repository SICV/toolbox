import json, cv2

try:
    FRAME_COUNT = cv2.CAP_PROP_FRAME_COUNT
    FRAME_WIDTH = cv2.CAP_PROP_FRAME_WIDTH
    FRAME_HEIGHT = cv2.CAP_PROP_FRAME_HEIGHT
except AttributeError:
    FRAME_COUNT = cv2.cv.CV_CAP_PROP_FRAME_COUNT
    FRAME_WIDTH = cv2.cv.CV_CAP_PROP_FRAME_WIDTH
    FRAME_HEIGHT = cv2.cv.CV_CAP_PROP_FRAME_HEIGHT

class Source ():
    def __init__(self, src, datasrc,init=True):
        self.src = src
        self.datasrc = datasrc
        if init:
            self.data = self.readdata(datasrc)
            self.numfaceframes = len(self.data)
            self.read_size()

    def readdata (self, src):
        ret = []
        with open(src) as f:
            for line in f:
                line = line.strip()
                if line and not line.startswith("#"):
                    data = json.loads(line)
                    ret.append(data)
                if line.startswith("---"):
                    break
        return ret

    def read_size (self):
        cap = cv2.VideoCapture(self.src)
        self.numframes = int(cap.get(FRAME_COUNT)) # this values seems to be too low
        self.width = int(cap.get(FRAME_WIDTH))
        self.height = int(cap.get(FRAME_HEIGHT))
        cap.release()

    def close (self):
        self.cap.release()

    def unparse (self):
        print "Movie with {0} frames sized {1}x{2}, {3} frames ({4:.1f}%) have faces".format(
            self.numframes, self.width, self.height, len(self.data), 100.0*(len(self.data)/float(self.numframes)))

    def features (self, feature_name="faces", with_frame=False):
        if with_frame:
            cap = cv2.VideoCapture(self.src)
        frameno = 0
        dataindex = 0
        while True:
            if with_frame:
                ret, frame = cap.read()
            if dataindex < len(self.data):
                # print dataindex, self.numfaceframes
                data = self.data[dataindex]
                if frameno == data["frameno"]:
                    for f in data["faces"]:
                        if with_frame:
                            yield (frameno, frame, f)
                        else:
                            yield (frameno, f)
                    dataindex += 1
            else:
                break
            frameno += 1
        if with_frame:
            cap.release()

    def faces (self, with_frame=False):
        return self.features("faces", with_frame=with_frame)

    def eyes (self, with_frame=False):
        return self.features("eyes", with_frame=with_frame)

    # def frames_with_features (self):
    #     cap = cv2.VideoCapture(self.src)
    #     frameno = 0
    #     dataindex = 0
    #     while True:
    #         ret, frame = cap.read()
    #         if dataindex < len(self.data):
    #             data = self.data[dataindex]
    #             if frameno == data["frameno"]:
    #                 yield (frameno, frame, data["face"], data["eyes"])
    #                 dataindex += 1
    #         frameno += 1
    #     cap.release()

    def frames (self, only_with_features=False):
        cap = cv2.VideoCapture(self.src)
        frameno = 0
        dataindex = 0
        while True:
            ret, frame = cap.read()
            faces, eyes = None, None
            if dataindex < len(self.data):
                data = self.data[dataindex]
                if frameno == data["frameno"]:
                    faces = data["faces"]
                    eyes = data["eyes"]
                    dataindex += 1

            if only_with_features:
                if faces:
                    yield (frameno, frame, faces, eyes)
            else:
                yield (frameno, frame, faces, eyes)

            frameno += 1
        cap.release()

