#!/usr/bin/env python

from argparse import ArgumentParser
from flat import rgb, font, shape, strike, document, text
import struct
from csv import DictReader


def decode_hexcolor (p):
    "#FFFFFF => (255, 255, 255)"
    if p.startswith("#"):
        p = p.lstrip("#")
    return struct.unpack('BBB',p.decode('hex'))

DEFAULT_BODY_FONT = '/var/www/vhosts/automatist.net/deptofreading/fonts/NimbusRomD.ttf'
DEFAULT_TEXT_COLOR = '#000000'
DEFAULT_TEXT_SIZE = 10.0
DEFAULT_TEXT_LEADING = 12.0
DEFAULT_LABEL_FONT = '/var/www/vhosts/automatist.net/deptofreading/fonts/NimbusSansD.ttf'
DEFAULT_LABEL_SIZE = 8
DEFAULT_LABEL_LEADING = 10.5
DEFAULT_FORMAT = "a4"
DEFAULT_LABEL = "Das Projekt einer Zeitschrift\nAbs. Am Flutgraben 3, 12435 Berlin"
DEFAULT_ADDRESSES = "/var/www/vhosts/automatist.net/deptofreading/addresses.csv"
DEFAULT_OUTPUT = "zeitschrift.pdf"

class Zeitschrift (object):
    def __init__(self):
        self.bodyfont = DEFAULT_BODY_FONT
        self.textcolor = DEFAULT_TEXT_COLOR
        self.textsize = DEFAULT_TEXT_SIZE
        self.textleading = DEFAULT_TEXT_LEADING
        self.labelfont = DEFAULT_LABEL_FONT
        self.labelsize = DEFAULT_LABEL_SIZE
        self.labelleading = DEFAULT_LABEL_LEADING
        self.format = DEFAULT_FORMAT
        self.label = DEFAULT_LABEL
        self.addresses = DEFAULT_ADDRESSES
        self.from_address = 1
        self.to_address = 1
        self.output = DEFAULT_OUTPUT
        self.textsrc = None
        self.text = None

    def make (self):
        zeitschrift(self)

def zeitschrift (args):
    textcolor = rgb(*decode_hexcolor(args.textcolor))
    bodyfont = font.open(args.bodyfont)
    labelfont = font.open(args.labelfont)

    # box = shape().stroke(textcolor).width(0.5)
    # headline = strike(font).color(black).size(20, 24)
    body = strike(bodyfont).color(textcolor).size(args.textsize, args.textleading)
    labelf = strike(labelfont).color(textcolor).size(args.labelsize, args.labelleading)

    if args.format == "a3":
        doc = document(400, 277, 'mm') # A3
        column_width = 90
        column_height = 267.5

    elif args.format == "a4":
        doc = document(200, 277, 'mm') # A4: 210 x 297 trimmed 10mm
        column_width = 90
        column_height = 267.5


    #############################
    # BODY
    #############################
    # each line is paragraph
    # insert blanks between each
    if args.text:
        src = args.text.splitlines()
    else:
        src = open(args.textsrc).readlines()
        src = [x.decode("utf-8") for x in src]
    paras = []
    for p in src:
        paras.append(body.paragraph(p))
        paras.append(body.paragraph(''))
    story = text(*paras)


    #############################
    # LABEL
    #############################
    # label = open(args.label).readlines()
    label = args.label.splitlines()
    label = [x.strip() for x in label if x.strip()]
    label = [labelf.paragraph(x) for x in label]
    label = text(*label)

    #from csv import DictReader
    #r = DictReader(open(args.addresses))
    addresses = []
    if type(args.addresses) == list:
        addresses = args.addresses
    else:
        # process csv
        for n, row in enumerate(DictReader(open(args.addresses))):
            if (n+1) >= args.from_address and (n+1) <= args.to_address:
                addresses.append([row['Name'], row['Address1'], row['Address2'], row['Address3']])

    for address in addresses:
        label_doc = document(column_height/2, column_width, 'mm')
        label_page = label_doc.addpage()
        label_page.place(label).frame(5, 5, column_height/2-10, column_width-10)
        # label_page.place(box.rect(5, 5, 90, 90))
        # label_doc.pdf("label.pdf")
        address = text(*[labelf.paragraph(x) for x in address])
        label_page.place(address).frame(69.25, 50, 100, 100)

        label_image = label_page.image(ppi=180, kind='rgb')
        label_image.rotate(False)

        # block = page.place(story).frame(p(5), p(5), p(90), p(267.5))
        done = False
        block = None

        page = doc.addpage()
        left = 5
        # block = page.place(story).frame(left, 5, column_width, column_height)
        block = page.place(story).frame(left, 5 + (column_height/2), column_width, (column_height/2))

        page.place(label_image).frame(0, 0, 90, column_height/2)
        #page.place(box.rect(5, 5, 90, column_height/2))

        columns = 4
        if args.format == "a4":
            columns = 2

        while block.overflow():
            for i in range(columns-1):
                left += 100
                block = page.chain(block).frame(left, 5, column_width, column_height)
                if not block.overflow():
                    break

            if block.overflow():
                left = 5
                page = doc.addpage()
                block = page.chain(block).frame(left, 5, column_width, column_height)

            # p.image(kind='rgb').png('hello.png')
            # d.page(0).svg('hello.svg')
    doc.pdf(args.output)

if __name__ == "__main__":

    parser = ArgumentParser(description='Make a Department of Reading PDF Zeitschrift from text source')
    parser.add_argument('--bodyfont', default=DEFAULT_BODY_FONT, help='body font')
    parser.add_argument('--textcolor', default=DEFAULT_TEXT_COLOR, help='Text color')
    parser.add_argument('--textsize', type=int, default=DEFAULT_TEXT_SIZE, help='Text size (mm?)')
    parser.add_argument('--textleading', type=int, default=DEFAULT_TEXT_LEADING, help='Text leading (mm?)')
    parser.add_argument('--labelfont', default=DEFAULT_LABEL_FONT, help='label font')
    parser.add_argument('--labelsize', type=float, default=DEFAULT_LABEL_SIZE, help='Label font size (mm?)')
    parser.add_argument('--labelleading', type=float, default=DEFAULT_LABEL_LEADING, help='Label font leading (mm?)')
    parser.add_argument('--format', default=DEFAULT_FORMAT, help='Format (a3, a4)')
    parser.add_argument('--label', default=DEFAULT_LABEL, help='File with return label (in lines)')
    parser.add_argument('--addresses', default=DEFAULT_ADDRESSES, help='File with addresses')
    parser.add_argument('--from-address', type=int, default=1, help='Start with address number (default: 1)')
    parser.add_argument('--to-address', type=int, default=1, help='End with address number (default: 1)')
    parser.add_argument('-o', '--output', default=DEFAULT_OUTPUT, help='Output file')

    parser.add_argument('--textsrc', default="zeitschrift.txt", help='Text source (filename)')
    parser.add_argument('--text', help='Text')
    parser.add_argument('--wikiurl', help='Text source (wikisrc)')

    args = parser.parse_args()

    if args.wikiurl:
        baseurl, sectionid = args.wikiurl.split("#")
        import html5lib, urllib2, sys
        from xml.etree import ElementTree as ET 
        import requests

        # f = urllib2.urlopen(baseurl)
        # src = f.read().decode("latin-1")
        src = requests.get(baseurl).text
        t = html5lib.parse(src, namespaceHTMLElements=False)
        div = t.find(".//div[@id='{0}']".format(sectionid))
        _text = ET.tostring(div, method="text", encoding="utf-8").decode("utf-8")
        _text = u"\n".join([x.strip() for x in _text.splitlines() if x.strip()])
        args.text = _text

    zeitschrift(args)
