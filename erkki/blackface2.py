from argparse import ArgumentParser
import sys, cv2, json
from numpy import zeros

p = ArgumentParser()
p.add_argument("src")
p.add_argument("--output", default="output.avi")
p.add_argument("--framerate", type=float, default=25, help="output frame rate")
p.add_argument("--frames", type=int, default=None, help="output only so many frames")
p.add_argument("--data", default="faces.json", help="json face data for src1")
p.add_argument("--fourcc", default="XVID", help="MJPG,mp4v,XVID")
p.add_argument("--shift", type=int, default=1)
args = p.parse_args()

try:
    fourcc = cv2.cv.CV_FOURCC(*args.fourcc)
except AttributeError:
    fourcc = cv2.VideoWriter_fourcc(*args.fourcc)

from source import Source

src = Source(args.src, args.data)
src.unparse()

out = cv2.VideoWriter()
out.open(args.output, fourcc, args.framerate, (src.width, src.height))

count = 0
for frameno, frame, faces, eyes in src.frames(only_with_features=True):
    print "output frame {0}".format(count+1)
    for x, y, w, h in faces:
        frame[y:y+h, x:x+w] = zeros((h,w,3), "uint8")
    out.write(frame)
    count += 1
    if args.frames and count >= args.frames:
        break

out.release()
