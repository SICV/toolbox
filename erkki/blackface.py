from argparse import ArgumentParser
import sys, cv2, json
from numpy import zeros

p = ArgumentParser()
p.add_argument("src")

p.add_argument("--output", default="output.avi")
p.add_argument("--framerate", type=float, default=25, help="output frame rate")
p.add_argument("--frames", type=int, default=None, help="output only so many frames")
p.add_argument("--data", default="faces.json", help="json face data for src1")
p.add_argument("--fourcc", default="XVID", help="MJPG,mp4v,XVID")
p.add_argument("--shift", type=int, default=1)
# p.add_argument("--xscale", default="624/720")
# p.add_argument("--yscale", default="480/540")
p.add_argument("--xscale", default="1.0")
p.add_argument("--yscale", default="1.0")
args = p.parse_args()

try:
    fourcc = cv2.cv.CV_FOURCC(*args.fourcc)
except AttributeError:
    fourcc = cv2.VideoWriter_fourcc(*args.fourcc)

from source import Source

src1 = Source(args.src, args.data)
src1.unparse()

out = cv2.VideoWriter()
out.open(args.output, fourcc, args.framerate, (src1.width, src1.height))

def parseScale (v):    
    if v:
        if "/" in v:
            n,d = v.split("/", 1)
            return float(n) / float(d)
        else:
            return float(v)
    else:
        return 1.0

xscale = parseScale(args.xscale)
yscale = parseScale(args.yscale)

print xscale, yscale


count = 0
for frameno, frame, face in src1.faces(with_frame=True):
    print "output frame {0}".format(count+1)
    # out.write(frame)

    # faceimg = frame[face[0]:face[1], face[0]+face[2]:face[1]+face[3]]
    x, y, w, h = face
    x = int(x*xscale)
    y = int(y*yscale)
    w = int(w*xscale)
    h = int(h*yscale)
    faceimg = frame[y:y+h, x:x+w]
    # faceimg = frame[x:x+w, y:y+h]
    # nf = cv2.resize(faceimg, (src1.width, src1.height))
    # out.write(nf)

    # newframe = zeros(frame.shape, "uint8")
    # newframe[y:y+h, x:x+w] = faceimg

    ## BLACK OUT FACES
    frame[y:y+h, x:x+w] = zeros((h,w,3), "uint8")
    out.write(frame)

    count += 1
    if args.frames and count >= args.frames:
        break

out.release()


# move face by face through src1, replacing with faces from 
# cut out
