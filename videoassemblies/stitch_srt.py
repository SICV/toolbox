from __future__ import print_function
from argparse import ArgumentParser
import numpy as np 
import cv2, sys, os
from matplotlib import pyplot as plt 
from srt import srtparse, timecode

# tutorial source: http://docs.opencv.org/3.0-beta/doc/py_tutorials/py_feature2d/py_feature_homography/py_feature_homography.html

def ensure_alpha (img, alpha=255):
    """
    takes (x, y, 3) 'uint8' shaped data and makes it
    (x, y, 4) with the given alpha, default: 255 (opaque)
    if data is already (x, y, 4) just returns it
    """
    if img.shape[2] < 4:
        alpha = np.ones((img.shape[0], img.shape[1], 4), 'uint8') * alpha
        alpha[:,:,:-1] = img
        return alpha
    return img

p = ArgumentParser("Stitch video frames onto a series of stills determined by an SRT format timeline of base images")
p.add_argument("--srt", default="matthew.srt", help="srt file")
p.add_argument("--frames", default="frames/frame%04d.jpg", help="input frames pattern, default: frames/frames%%04d.jpg")
p.add_argument("--min-features", type=int, default=10, help="minimum matching features, default: 10")
p.add_argument("--output", default="frames_warp/frame%04d.png", help="output image, default: output.png")
p.add_argument("--fps", type=float, default=25.0, help="fps, default: 25.0")
p.add_argument("--gap", type=float, default=0.0, help="time to put black between frames, default: 0.0")
p.add_argument("--duration", type=float, default=None, help="total duration, only used with --gap for last title")
p.add_argument("--fixed", default="originals/", help="prefix fixed framenames (path)")
p.add_argument("--force", default=False, action="store_true", help="force even if already generated")
# p.add_argument("--gray", default=False, action="store_true", help="gray scale output, default: false")
p.add_argument("--verbose", default=False, action="store_true")
args = p.parse_args()

with open(args.srt) as f:
    titles = srtparse(f.read())

curtitle = -1
framenum = 1
fixedname = None
fixed_image = None
kp2, des2 = None, None
black_frame = None
width, height = None, None

FLANN_INDEX_KDTREE = 0
index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees=5)
search_params = dict(checks = 50)
# http://docs.opencv.org/modules/features2d/doc/common_interfaces_of_descriptor_matchers.html?highlight=flannbasedmatcher#flannbasedmatcher
reload_fixed_image = False

while True:
    sift = cv2.xfeatures2d.SIFT_create()
    flann = cv2.FlannBasedMatcher(index_params, search_params)

    framepath = args.frames % framenum
    if not os.path.exists(framepath):
        break
    t = ((framenum-1) / args.fps)
    while ((curtitle+1) < len(titles)) and (titles[curtitle+1]['start'] <= t):
        curtitle += 1
        if args.verbose:
            print ("select title", titles[curtitle], file=sys.stderr)
        fixedname = titles[curtitle]['content'].strip()
        fixedpath = os.path.join(args.fixed, fixedname)
        reload_fixed_image = True

    outframepath = args.output % framenum
    black = False
    if args.gap > 0:
        end = args.duration
        if 'end' in titles[curtitle]:
            end = titles[curtitle]['end']
        black = end and (t + args.gap) >= end

    # STATUS MESSAGE
    print ("{0} {1} + {2} => {3}".format(timecode(t), fixedname, framepath, outframepath), file=sys.stderr)

    # CREATE THE FRAME
    framenum += 1
    if not args.force and os.path.exists(outframepath) and os.path.getsize(outframepath) > 0:
        if args.verbose:
            print ("Skipping already generated frame {0}".format(outframepath), file=sys.stderr)
        continue

    if reload_fixed_image:
        if args.verbose:
            print ("Reading fixed image {0}".format(fixedpath), file=sys.stderr)
        reload_fixed_image = False
        fixed_image = cv2.imread(fixedpath,0) # training
        # fixed_image_color = cv2.imread(fixedpath)
        kp2, des2 = sift.detectAndCompute(fixed_image, None)
        if black_frame == None:
            width = fixed_image.shape[1]
            height = fixed_image.shape[0]
            if args.verbose:
                print ("Output frame size: {0}x{1}".format(width, height), file=sys.stderr)
            black_frame = np.zeros((height, width, 4), 'uint8')

    wrote_output = False
    if args.verbose:
        print ("Reading frame {0}".format(framepath), file=sys.stderr)
    place_image = cv2.imread(framepath,0) # query image
    place_image_color = ensure_alpha(cv2.imread(framepath,-1))
    kp1, des1 = sift.detectAndCompute(place_image,None)
    matches = flann.knnMatch(des1,des2,k=2) # order: query, training
    good = []
    for m, n in matches:
        if m.distance < 0.7*n.distance:
            good.append(m)

    if len(good) >= args.min_features:
        # note: reshape -1 means whatever it needs to be to fit all data
        if args.verbose:
            print ("Found {0} matching features".format(len(good)), file=sys.stderr)
        src_pts = np.float32([ kp1[m.queryIdx].pt for m in good ]).reshape(-1, 1, 2)
        dst_pts = np.float32([ kp2[m.trainIdx].pt for m in good ]).reshape(-1, 1, 2)

        M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 5.0)
        if M != None:
            # http://docs.opencv.org/modules/imgproc/doc/geometric_transformations.html#warpperspective
            warped_image = cv2.warpPerspective(place_image_color, M, fixed_image.shape)
            cv2.imwrite(outframepath, warped_image)
            wrote_output = True
        else:
            print ("findHomography failed", file=sys.stderr)    

    else:
        print ("Not enough features match ({0}/{1})".format(len(good), args.min_features), file=sys.stderr)    


    if not wrote_output:        
        # OUTPUT A BLACK FRAME
        cv2.imwrite(outframepath, black_frame)
