#!/usr/bin/env python

from sh import git
import json
import cgi

fs = cgi.FieldStorage()
cmd = fs.getvalue("cmd", "log")
_id = fs.getvalue("id")


if cmd == "log":
    log = git("-c", "core.pager=cat", "log", "--oneline", "--no-color")
    print "Content-type: application/json"
    print
    print json.dumps([x.split(" ", 1) for x in reversed(log.splitlines())])
elif cmd == "ls":
    log = git("ls-tree", _id)
    print "Content-type: application/json"
    print
    print json.dumps([x.split(None, 3) for x in log.splitlines()])
elif cmd == "cat":
    data = git("cat-file", "blob", _id)
    print "Content-type: text/plain"
    print
    sys.stdout.write(data)

# interrogation commands
# git-cat-file
# git-diff-files,index,tre
# git-ls-files,tree (ls-files: index, working tree)

# git-status --porcelain