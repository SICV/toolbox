from argparse import ArgumentParser
from PIL import Image
from PCV.localdescriptors.sift import read_features_from_file, match_twosided
import sys
from PIL.ImageDraw import ImageDraw

"""
python filters/stitch.py --i1 frames/00007_00-20-02-254.jpg --f1 frames/00007_00-20-02-254.sift --i2 originals/S-20-28-38_1080.jpg --f2 sift/S-20-28-38_1080.jpg.sift
"""

p = ArgumentParser("")
p.add_argument("--f1")
p.add_argument("--f2")
p.add_argument("--i1")
p.add_argument("--i2")
p.add_argument("--imgext", default="jpg")
args = p.parse_args()

img1 = Image.open(args.i1)
img2 = Image.open(args.i2)

# w = max(img1.size[0], img2.size[0])
# h = max(img1.size[1], img2.size[1])

gap = 50

w = (img1.size[0] + img2.size[0] + gap)
h = max(img1.size[1], img2.size[1])

out = Image.new("RGB", (w, h))
print out.size
out.paste(img1, (0, 0))
dx = img1.size[0] + gap
out.paste(img2, (dx, 0))

draw = ImageDraw(out)
# draw.line((0, 0, 100, 100), width=1, fill=(255, 255, 0))
# out.save("output.png")
# sys.exit(0)

l1,d1 = read_features_from_file(args.f1)
l2,d2 = read_features_from_file(args.f2)
matches = match_twosided(d1, d2)

for i, m in enumerate(matches):
	if m > 0:
		x1, y1, s1, r1 = l1[i]
		x2, y2, s2, r2 = l2[m]
		draw.line((x1, y1, x2+dx, y2), width=1, fill=(255, 255, 0))
		print "scales", s1, s2, (s1/s2)
		print "r", r1, r2, (r1-r2)
		print


out.save("output.png")
