from __future__ import print_function
from argparse import ArgumentParser
from PCV.localdescriptors.sift import read_features_from_file, match_twosided
from numpy import sum
import sys

p = ArgumentParser("")
p.add_argument("--match")
p.add_argument("input", nargs="*", default=[])
args = p.parse_args()

print ("Matches for {0}".format(args.match))
l1,d1 = read_features_from_file(args.match)

matchscores = []

for i in args.input:
	l2,d2 = read_features_from_file(i)
	print (i, file=sys.stderr)
	matches = match_twosided(d1, d2)
	nbr_matches = sum(matches > 0)
	if nbr_matches > 0:
		print ("   {0}".format(nbr_matches), file=sys.stderr)
		matchscores.append((nbr_matches, i))

matchscores.sort()

for n, i in matchscores:
	print (n, i)
