from PCV.imagesearch.imagesearch import Searcher
from PCV.localdescriptors.sift import read_features_from_file
from argparse import ArgumentParser
import pickle
from numpy import sqrt

p = ArgumentParser()
p.add_argument("input", help="SIFT input to search db for")
p.add_argument("--vocab", default="vocab.pkl")
p.add_argument("--db", default="index.db")
args = p.parse_args()

with open(args.vocab, 'rb') as f:
	voc = pickle.load(f)

src = Searcher(args.db, voc)
locs, desc = read_features_from_file(args.input)
iw = voc.project(desc)

def query (src, h):
	candidates = src.candidates_from_histogram(h)
	matchscores = []
	for imid in candidates:
		cand_name = src.con.execute("select filename from imlist where rowid=%d" % imid).fetchone()[0]
		cand_h = src.get_imhistogram(cand_name)
		cand_dist = sqrt(sum( (h-cand_h) ** 2))
		matchscores.append((cand_dist, imid, cand_name))
	matchscores.sort()
	return matchscores

print "<h1>results</h1>"
results = query(src, iw)
for d, imgid, imgname in results:
	print """<div class="result">
		d: {0}<br />
		<img src="originals/{1}" />
	</div>""".format(d, imgname)
