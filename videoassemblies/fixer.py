from argparse import ArgumentParser
import os

p = ArgumentParser("")
p.add_argument("--pattern", default="frames_composed/frame%04d.png")
p.add_argument("--start", type=int, default=875)
p.add_argument("--duration", type=float)
p.add_argument("--fps", type=float, default=25.0, help="fps, default: 25.0")
p.add_argument("--command")
args = p.parse_args()

framenum = args.start

while True:
    t = ((framenum-1) / args.fps)
    if t >= args.duration:
        break

    p = args.pattern % framenum
    if os.path.exists(p):
        cmd = args.command % p
        print cmd

    framenum += 1


