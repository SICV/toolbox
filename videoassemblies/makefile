#####################
# VARIABLES
#####################
# The variables of a makefile determine the precise names of files TO BE GENERATED
# the make file is essentially driven by these variables

# Find any images in the originals folder to use as originals (NB basenames must be unique)
originals = $(shell find originals/ -iname "*.jpg")
# basenames = $(basename $(notdir $(originals)))
basenames = $(notdir $(originals))

sift_images=$(addprefix sift/, $(addsuffix .png, $(basenames)))
# sift_json = $(sift_images:%.png=%.json)

### ADD new filter variables above and to the lists below...

derivs = $(sift_images)
# json = $(sift_json)

#####################
# thumbs = $(derivs:%.png=%.thumb.png)
# icons = $(derivs:%.png=%.icon.png)


############################
# RULES & RECIPES
############################
# Generic Rules & Recipes

# default rule
all: $(derivs)

# Use imagemagick to make thumbs & icons
# Important for these rules to be first
# otherwise it attempts to make thumbs by applying filters to preview thumbs & icons :(
# %.thumb.png: %.png
# 	convert -resize 320x $< $@

# %.icon.png: %.png
# 	convert -resize 32x $< $@

# Use exiftool to extract json from an image
# %.json: %.png
# 	exiftool -json $< > $@

# %.html: %.md 
# 	pandoc --from markdown --to html --standalone -o $@ $<

# compilemeta accumulates an index of all the json files
# index.json: $(json)
# 	python compilemeta.py $^ > $@

# Use make print-NAME to debug variables
# as in: make print-originals
print-%:
	@echo '$*=$($*)'

# Use make clean to force a regeneration of everything
clean:
	rm -rf sift

sift/%.png: originals/%
	mkdir -p sift
	python filters/sift.py $< --output sift/$*.sift --draw $@.svg --count > annotate.tmp
	convert -background transparent $@.svg $@
	mogrify -trim $@
	# exiftool -MakerNote=sift "-UserComment<=annotate.tmp" -OriginalRawFileName=$* $@ -overwrite_original
	rm annotate.tmp

sift = $(shell ls sift/*.sift)
frames = $(shell ls frames/shot*.png)
frames_sift = $(frames:%.png=%.sift)
frames_matches = $(frames:%.png=%.matches)

.PHONY: frames_sift frames_matches frames

frames_sift: $(frames_sift)

frames_matches: $(frames_matches)

%.sift: %.jpg
	python filters/sift.py $< --draw $@.svg

# %.sift: %.png
# 	python filters/sift.py $< --draw $@.svg

%.matches: %.sift $(sift)
	python filters/matchdescriptors.py --match $< $(sift) > $@


frames:
	ffmpeg -i matthew.webm -vf fps=25 frames/frame%04d.jpg

frames_svg:
	mkdir -p frames_svg
	python svg_draw_srt.py \
		--srt matthew.srt \
		--strip "_1080.jpg" \
		--duration 300 \
		--input contours/%s_540.svg \
		--output frames_svg/frame%04d.svg \
		--verbose  

frames_svg = $(shell ls frames_svg/*.svg)
frames_svg_png = $(frames_svg:%.svg=%.png)

%.png: %.svg
	inkscape --export-png=$@ --export-width=1080 --export-height=1080 $<

frames_svg_png: $(frames_svg_png)

compose:
	python compose_frames.py \
		--srt matthew.srt \
		--duration 300 \
		--gap 5.0 \
		--output frames_composed/frame%04d.png

output.webm:
	~/src/ffmpeg/ffmpeg \
	    -framerate 25 \
	    -i frames_composed/frame%04d.png \
	    -i matthew.wav \
	    -r 25 \
	    -c:v libvpx \
	    -crf 4 -b:v 2M \
	    -c:a libvorbis \
	    -shortest \
	    -threads 4 \
	    output.webm -y



