from __future__ import print_function
from argparse import ArgumentParser
import sys, os
from srt import srtparse, timecode
from contoursvg import ContourSVG


p = ArgumentParser("Draw SVG as a series of stills determined by an SRT format timeline")
p.add_argument("--srt", default="matthew.srt", help="srt file")
p.add_argument("--strip", default="_1080.jpg", help="strip this from srt filesnames")
p.add_argument("--input", default="contours/%s_540.svg", help="svg input frames pattern")
p.add_argument("--output", default="frame%04d.svg", help="output svg pattern")
p.add_argument("--fps", type=float, default=25.0, help="fps, default: 25.0")
p.add_argument("--gap", type=float, default=0.0, help="time to put black between frames, default: 0.0")
p.add_argument("--duration", type=float, default=None, help="total duration, only used with --gap for last title")
p.add_argument("--force", default=False, action="store_true", help="force even if already generated")
# p.add_argument("--gray", default=False, action="store_true", help="gray scale output, default: false")
p.add_argument("--verbose", default=False, action="store_true")
args = p.parse_args()

with open(args.srt) as f:
    titles = srtparse(f.read())

curtitle = -1
framenum = 1

width, height = None, None
svgpath = None
svg = None
reload_svg = False
t = 0

while True:
    t = ((framenum-1) / args.fps)
    if t >= args.duration:
        break
    while ((curtitle+1) < len(titles)) and (titles[curtitle+1]['start'] <= t):
        curtitle += 1
        # if args.verbose:
        #     print ("select title", titles[curtitle], file=sys.stderr)
        fixedname = titles[curtitle]['content'].strip()
        if fixedname.endswith(args.strip):
            fixedname = fixedname[:-len(args.strip)]
        svgpath = args.input % fixedname
        reload_svg = True

    outframepath = args.output % framenum

    start = titles[curtitle]['start']
    if 'end' in titles[curtitle]:
        end = titles[curtitle]['end']
    else:
        end = args.duration
    if args.gap:
        end -= args.gap

    p = max(0.0, min(1.0, float(t - start) / (end - start)))

    if args.verbose:
        print ("{0}: {1} {2:0.02f} => {3}".format(timecode(t), svgpath, p, outframepath), file=sys.stderr)

    framenum += 1

    # CREATE THE FRAME

    if not args.force and os.path.exists(outframepath) and os.path.getsize(outframepath) > 0:
        if args.verbose:
            print ("Skipping already generated frame {0}".format(outframepath), file=sys.stderr)
        continue

    if reload_svg:
        if args.verbose:
            print ("Reading SVG {0}".format(svgpath), file=sys.stderr)
        reload_svg = False
        svg = ContourSVG(svgpath)

    # imwrite(outframepath)
    svg.save(outframepath, p)