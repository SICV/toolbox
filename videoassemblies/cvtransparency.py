import cv2, sys, os
import numpy as np 

width, height = 1080, 1080
black = np.zeros((height, width, 4), 'uint8')
cv2.imwrite("blank.png", black)
sys.exit()


def ensure_alpha (img):
	if img.shape[2] < 4:
		alpha = np.ones((img.shape[0], img.shape[1], 4), 'uint8') * 255
		alpha[:,:,:-1] = img
		return alpha
	return img

FLANN_INDEX_KDTREE = 0
index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees=5)
search_params = dict(checks = 50)

sift = cv2.xfeatures2d.SIFT_create()
flann = cv2.FlannBasedMatcher(index_params, search_params)

place_image = cv2.imread("frames/frame0001.jpg",0)
place_image_color = ensure_alpha(cv2.imread("frames/frame0001.jpg",-1))
# place_image_color = cv2.imread("frames/frame0001.png",-1)
print place_image_color.shape

fixed_image = cv2.imread("originals/S-20-28-38_1080.jpg",0) # training

kp1, des1 = sift.detectAndCompute(place_image,None)
kp2, des2 = sift.detectAndCompute(fixed_image, None)

matches = flann.knnMatch(des1,des2,k=2) # order: query, training
good = []
for m, n in matches:
    if m.distance < 0.7*n.distance:
        good.append(m)

if len(good) < 10:
	print "not enough matches"
	sys.exit()

src_pts = np.float32([ kp1[m.queryIdx].pt for m in good ]).reshape(-1, 1, 2)
dst_pts = np.float32([ kp2[m.trainIdx].pt for m in good ]).reshape(-1, 1, 2)

M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 5.0)
if M == None:
	print "findHomography failed"
	sys.exit()

if M != None:
    # http://docs.opencv.org/modules/imgproc/doc/geometric_transformations.html#warpperspective
    warped_image = cv2.warpPerspective(place_image_color, M, fixed_image.shape)
    cv2.imwrite("output.png", warped_image)

