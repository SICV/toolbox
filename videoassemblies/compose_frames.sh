mkdir -p output_composed
for i in ls output/*.png
do
# echo $i
b=`basename $i`
b=${b%.*}
convert $i output_contours/$b.png -compose Hard_Light -composite -compose Over -background black -gravity center -extent 1920x1080 output_composed/$b.png
echo $b
done
