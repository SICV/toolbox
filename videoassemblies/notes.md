https://opencv-python-tutroals.readthedocs.org/en/latest/py_tutorials/py_feature2d/py_feature_homography/py_feature_homography.html#feature-homography

So, did get OpenCV to compile using extras.

* https://github.com/Itseez/opencv
* https://github.com/Itseez/opencv_contrib
* http://docs.opencv.org/doc/tutorials/introduction/linux_install/linux_install.html
* https://github.com/Itseez/opencv/archive/3.0.0.zip

In the end I used:

    wget https://github.com/Itseez/opencv/archive/3.0.0.zip
    git clone https://github.com/Itseez/opencv_contrib

I needed to uninstall my manually compiled ffmpeg (conflict with libav libs I think... which were not built to be shared).

Ultimately:

	mkdir release
	cd release
    cmake -D DMAKE_BUILD_TYPE=RELEASE -DOPENCV_EXTRA_MODULES_PATH=/home/murtaugh/opencv/opencv_contrib/modules/ ..
    make

Question: has SIFT & co been renamed in 3.0.0 so that instead of cv2.SIFT() it's cv2.xfeatures2d.SIFT_create() ?!

Hey and there's [ORB](http://opencv-python-tutroals.readthedocs.org/en/latest/py_tutorials/py_feature2d/py_orb/py_orb.html) as a free alternative to SIFT!

([Computational Photography](http://opencv-python-tutroals.readthedocs.org/en/latest/py_tutorials/py_photo/py_table_of_contents_photo/py_table_of_contents_photo.html))

Composing each frame
=======================
Imagemagick is used to overlay the SVG frame with the opencv output using a "Hard Light" layering. In addition, the frame is centered in an HD size (1920x1080). It's important to reset the compose mode to the default "Over" setting before using extent to resize the canvas (otherwise the Hard_Light compose is used again against the new black background).

    convert output/frame0001.png \
    	output_contours/$b.png \
    	-compose Hard_Light \
    	-composite \
    	-compose Over \
    	-background black \
    	-gravity center \
    	-extent 1920x1080 \
    	output_composed/$b.png

From imagemagick's [usage page about image composition](http://www.imagemagick.org/Usage/compose/#compose_operators):

	> Apart from the direct two image compositing styles shown above, there are a number of other image operations that also use alpha compositing as part of their internal image processing. These operations are affected by the current "-compose" setting, though they will use either their own internal positioning, or a Layered Image virtual canvas offset positioning technique.



Frames + Audio = Video
=======================
Finally, ffmpeg is used to take the final frames and combine them with audio in a webm video.

* [ffmpeg: Create a video slideshow from images](https://trac.ffmpeg.org/wiki/Create%20a%20video%20slideshow%20from%20images)

	~/src/ffmpeg/ffmpeg \
	    -framerate 25 \
	    -i output_composed/frame%04d.png \
	    -i matthew.wav \
	    -r 25 \
	    -c:v libvpx \
	    -crf 4 -b:v 2M \
	    -c:a libvorbis \
	    -shortest \
	    output.webm -y



A situated note on the power of the pipeline
==============================================

Why writing a script to generate another script is powerful. The example of fixblack.py: By outputting the commands necessary to "fix" the problem of the broken black frames (which in this case are files of exactly 73 bytes in size) -- the program is both an indicator (check for broken files) as well as eventually the fix (when one pipes this then to bash). By using a pipeline, it allows two modes of operation -- running the first part as a check, then connecting it together to actually do it -- in this way you can check the intermediate stage before blindly performing the fixing stage and in fact you have a more diverse set of operations which can be composed more fluidly in response to a given situation.

Verbose mode:

	Reading frame frames/frame6051.jpg
	Found 113 matching features
	00:04:02,040 S-20-9-32_1080.jpg + frames/frame6052.jpg => frames_warp/frame6052.png
	Reading frame frames/frame6052.jpg
	Found 112 matching features
	00:04:02,080 S-20-9-32_1080.jpg + frames/frame6053.jpg => frames_warp/frame6053.png
	Reading frame frames/frame6053.jpg
	Found 118 matching features
	00:04:02,120 S-20-9-32_1080.jpg + frames/frame6054.jpg => frames_warp/frame6054.png
	Reading frame frames/frame6054.jpg
	Found 110 matching features
	00:04:02,160 S-20-9-32_1080.jpg + frames/frame6055.jpg => frames_warp/frame6055.png
	Reading frame frames/frame6055.jpg
	Found 114 matching features
	00:04:02,200 S-20-9-32_1080.jpg + frames/frame6056.jpg => frames_warp/frame6056.png
	Reading frame frames/frame6056.jpg
	Found 135 matching features
	00:04:02,240 S-20-9-32_1080.jpg + frames/frame6057.jpg => frames_warp/frame6057.png
	Reading frame frames/frame6057.jpg
	Found 119 matching features

At the same time interesting to see how the scripts I've written really work best when I fix the output to a single line "status" display a la:

	00:04:02,040 S-20-9-32_1080.jpg + frames/frame6052.jpg => frames_warp/frame6052.png
	00:04:02,080 S-20-9-32_1080.jpg + frames/frame6053.jpg => frames_warp/frame6053.png
	00:04:02,120 S-20-9-32_1080.jpg + frames/frame6054.jpg => frames_warp/frame6054.png
	00:04:02,160 S-20-9-32_1080.jpg + frames/frame6055.jpg => frames_warp/frame6055.png
	00:04:02,200 S-20-9-32_1080.jpg + frames/frame6056.jpg => frames_warp/frame6056.png
	00:04:02,240 S-20-9-32_1080.jpg + frames/frame6057.jpg => frames_warp/frame6057.png

Which then allows me to run (in parallel):

	00:01:07,760: originals/S-20-8-3_1080.jpg + frames_svg/frame1695.png + frames_warp/frame1695.png => frames_composed/frame1695.png
	00:01:07,800: originals/S-20-8-3_1080.jpg + frames_svg/frame1696.png + frames_warp/frame1696.png => frames_composed/frame1696.png
	00:01:07,840: originals/S-20-8-3_1080.jpg + frames_svg/frame1697.png + frames_warp/frame1697.png => frames_composed/frame1697.png
	00:01:07,880: originals/S-20-8-3_1080.jpg + frames_svg/frame1698.png + frames_warp/frame1698.png => frames_composed/frame1698.png
	00:01:07,920: originals/S-20-8-3_1080.jpg + frames_svg/frame1699.png + frames_warp/frame1699.png => frames_composed/frame1699.png
	00:01:07,960: originals/S-20-8-3_1080.jpg + frames_svg/frame1700.png + frames_warp/frame1700.png => frames_composed/frame1700.png
	00:01:08,000: originals/S-20-8-3_1080.jpg + frames_svg/frame1701.png + frames_warp/frame1701.png => frames_composed/frame1701.png
	00:01:08,040: originals/S-20-8-3_1080.jpg + frames_svg/frame1702.png + frames_warp/frame1702.png => frames_composed/frame1702.png
	00:01:08,080: originals/S-20-8-3_1080.jpg + frames_svg/frame1703.png + frames_warp/frame1703.png => frames_composed/frame1703.png
	00:01:08,120: originals/S-20-8-3_1080.jpg + frames_svg/frame1704.png + frames_warp/frame1704.png => frames_composed/frame1704.png
	00:01:08,160: originals/S-20-8-3_1080.jpg + frames_svg/frame1705.png + frames_warp/frame1705.png => frames_composed/frame1705.png
	00:01:08,200: originals/S-20-8-3_1080.jpg + frames_svg/frame1706.png + frames_warp/frame1706.png => frames_composed/frame1706.png
	00:01:08,240: originals/S-20-8-3_1080.jpg + frames_svg/frame1707.png + frames_warp/frame1707.png => frames_composed/frame1707.png

It's about iterrogability and performativity. It's about using our power of reading and writing and to use tools performatively in response.

The reduced "verbose" mode allows the timing of two processes to be better readable and gives the insight that the two processes can occur in parallel (as long as the composer doesn't catch up to the warped frame generator).

But also adding the "preflight" check to the composer coupled with the "skip existing" behaviour allows the script to be run ahead of time and repeated as necessary. This adds a lot to script performativity.

	Makes scripts repeatable
	Having scripts that gracefully recover where they left off means the occasionaly Segmentation Fault is not problem, just restart where you left off. This is also about respecting context (looking at the file system to see what's already been done).
	

