from __future__ import print_function
from argparse import ArgumentParser
import sys, os
from srt import srtparse, timecode
from time import sleep

p = ArgumentParser("Draw SVG as a series of stills determined by an SRT format timeline")
p.add_argument("--srt", default="matthew.srt", help="srt file")
p.add_argument("--originals", default="originals/%s", help="pattern to compose the full path of input images referenced in the srt")
p.add_argument("--svg", default="frames_svg/frame%04d.png", help="")
p.add_argument("--warp", default="frames_warp/frame%04d.png", help="")
p.add_argument("--output", default="frames_composed/frame%04d.png", help="")

p.add_argument("--fps", type=float, default=25.0, help="fps, default: 25.0")
p.add_argument("--gap", type=float, default=0.0, help="time to put black between frames, default: 0.0")
p.add_argument("--duration", type=float, default=None, help="total duration, only used with --gap for last title")

p.add_argument("--force", default=False, action="store_true", help="force even if already generated")
p.add_argument("--verbose", default=False, action="store_true")

args = p.parse_args()

with open(args.srt) as f:
    titles = srtparse(f.read())

t = 0
curtitle = -1
framenum = 1
fixedpath = None

while True:
    t = ((framenum-1) / args.fps)
    if t >= args.duration:
        break
    while ((curtitle+1) < len(titles)) and (titles[curtitle+1]['start'] <= t):
        curtitle += 1
        fixedpath = args.originals % titles[curtitle]['content'].strip()

    outframepath = args.output % framenum
    svgpath = args.svg % framenum
    warppath = args.warp % framenum

    start = titles[curtitle]['start']
    if 'end' in titles[curtitle]:
        end = titles[curtitle]['end']
    else:
        end = args.duration

    black = (args.gap > 0.0) and (t + args.gap >= end)

    # STATUS
    use_fixed = fixedpath
    if black:
        use_fixed = "(Black frame)"
    print ("{0}: {1} + {2} + {3} => {4}".format(timecode(t), use_fixed, svgpath, warppath, outframepath), file=sys.stderr)

    framenum += 1

    # CREATE THE FRAME

    if not args.force and os.path.exists(outframepath) and os.path.getsize(outframepath) > 0:
        print ("Skipping already generated frame {0}".format(outframepath), file=sys.stderr)
        continue

    if os.path.exists(fixedpath) and os.path.exists(svgpath) and os.path.exists(warppath):

        use_fixed = fixedpath
        if black:
            use_fixed = "-size 1080x1080 canvas:black"
        cmd = """convert {0} {1} -compose Hard_Light -composite {2} -compose Over -composite {3}"""
        cmd = cmd.format(use_fixed, svgpath, warppath, outframepath)
        os.system(cmd)
    else:
        print ("Missing input file, skipping", file=sys.stderr)

    sleep(0.01)