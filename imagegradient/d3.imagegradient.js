(function () {
  function image_gradient_viewer (elt) {
    var MODE_LIVE = 0,
      MODE_GRID = 1,
      MODE_IMAGE = 2,
      dis = d3.select(this)
            .style("position", "relative")
            .style("display", "inline-block"),
        img = dis.append("img")
            .attr("src", function (d) { return d.src })
            .on("load", init_with_size),
        gimg, canvas, ctx, image_data,
        magScale = 100.0,
        grid_size = 20.0,
        width, height, ready = false,
        display_mode = MODE_LIVE;

    function init_with_size () {
      // console.log("loaded");
      width = img[0][0].width;
      height = img[0][0].height;

      // console.log("load", w, h);
      canvas = dis.append("canvas")
        .style("position", "absolute")
        .attr("class", "imagegradient_draw")
        // .style("display", "none")
        .style("left", "0px")
        .style("top", "0px")
        .attr("width", width)
        .attr("height", height)
        .on("mousemove", function (d) {
          if (!ready || display_mode!==MODE_LIVE) return;
          var e = d3.event;
          var mp = getMousePos(this, e);
          // console.log("mp", mp);
          ctx.clearRect(0, 0, width, height);
          draw_gradient(mp.x, mp.y);
        })
        .on("mouseleave", function () {
          if (display_mode!==MODE_LIVE) return;
          ctx.clearRect(0, 0, width, height);
        })
        .on("click", function (d) {
          d3.event.preventDefault();
          display_mode += 1;
          if (display_mode > MODE_IMAGE) display_mode = 0;
          if (display_mode == MODE_GRID) {
            draw_grid();
          } else if (display_mode == MODE_IMAGE) {
            draw_image();
          } else {
            ctx.clearRect(0, 0, width, height);
          }
        });

      ctx = canvas[0][0].getContext("2d");
      ctx.lineWidth = 4;
      ctx.strokeStyle = "rgba(0,0,255,1)";

      gimg = dis.append("img")
        .attr("class", "imagegradient_gradient")
        .style("display", "none")
        .attr("src", function (d) { return d.gradient_src })
        .style("position", "absolute")
        .style("left", "0px")
        .style("top", "0px")
        .style("display", "none")
        .on("load", function () {
          ctx.drawImage(gimg[0][0], 0, 0);
          image_data = ctx.getImageData(0, 0, width, height);
          ctx.clearRect(0, 0, width, height);
          ready = true;
        });
    }

    function draw_grid () {
      ctx.clearRect(0, 0, width, height);
      for (var y=0; y<height; y+= grid_size) {
          for (var x=0; x<width; x+= grid_size) {
              draw_gradient(x, y); } }
    }

    function draw_image () {
      ctx.clearRect(0, 0, width, height);
      ctx.drawImage(gimg[0][0], 0, 0);
    }

    function draw_gradient(x, y) {
        var d = image_data.data,
          id_offset = ((y*image_data.width)+x)*4,
          rgb = d3.rgb(d[id_offset+0], d[id_offset+1], d[id_offset+2]),
          hsl = rgb.hsl(),
          hue = isNaN(hsl.h) ? 0 : hsl.h,
          level = isNaN(hsl.l) ? 0 : hsl.l,
          g_angle = (hue/360.0) * Math.PI * 2.0,
          g_mag = level * magScale,
          dy = -Math.cos(g_angle) * g_mag,
          dx = Math.sin(g_angle) * g_mag;
        ctx.beginPath();
        canvas_arrow(ctx, x, y, x+dx, y+dy);
        ctx.closePath();
        ctx.stroke();
    }
  }

  //http://stackoverflow.com/questions/808826/draw-arrow-on-canvas-tag
  function canvas_arrow(context, fromx, fromy, tox, toy){
    var headlen = 10;   // length of head in pixels
    var angle = Math.atan2(toy-fromy,tox-fromx);
    context.moveTo(fromx, fromy);
    context.lineTo(tox, toy);
    context.lineTo(tox-headlen*Math.cos(angle-Math.PI/6),toy-headlen*Math.sin(angle-Math.PI/6));
    context.moveTo(tox, toy);
    context.lineTo(tox-headlen*Math.cos(angle+Math.PI/6),toy-headlen*Math.sin(angle+Math.PI/6));
  }

  function getMousePos(canvas, evt) {
      var rect = canvas.getBoundingClientRect();
      return {
        x: evt.clientX - rect.left,
        y: evt.clientY - rect.top
      };
  }
  window.image_gradient_viewer = image_gradient_viewer; 
})();