Usage
-----------

```bash
python python/imagegradient.py foo.png --output foo.gradient.png --svg foo.gradient.svg
```

Converts foo.png (input) into two outputs.

