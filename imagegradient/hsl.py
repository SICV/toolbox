# Based on implementation in d3.js
import random

def hsl_to_rgb (h, s, l):
    # fix ranges for hsl values
    if h == None:
        h = 0
    if h < 0 or h > 360:
        h = h % 360
    if s < 0:
        s = 0
    s = max(0.0, min(1.0, s))
    l = max(0.0, min(1.0, l))

    # From FvD 13.37, CSS Color Module Level 3
    if l <= .5:
        m2 = l * (1 + s)
    else:
        m2 = l + s - l * s
    m1 = 2 * l - m2

    def v(h):
        if (h > 360):
            h -= 360
        elif (h < 0):
            h += 360
        if (h < 60):
            return m1 + (m2 - m1) * h / 60
        if (h < 180):
            return m2
        if (h < 240):
            return m1 + (m2 - m1) * (240 - h) / 60
        return m1

    def vv(h):
        return round(v(h) * 255)

    return (int(vv(h + 120)), int(vv(h)), int(vv(h - 120)))


if __name__ == "__main__":
    print hsl_to_rgb(0, 1.0, 0.5)
    print hsl_to_rgb(120, 1.0, 0.5)
    print hsl_to_rgb(240, 1.0, 0.5)

    for i in range(100):
        print hsl_to_rgb(random.random()*360, random.random(), random.random())
