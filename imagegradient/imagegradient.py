#!/usr/bin/python

from __future__ import print_function
from PIL import Image, ImageDraw
from numpy import *
from scipy.ndimage import filters
import sys, json, os, math
import argparse


def hsl_to_rgb (h, s, l):
    # fix ranges for hsl values
    if h == None:
        h = 0
    if h < 0 or h > 360:
        h = h % 360
    if s < 0:
        s = 0
    s = max(0.0, min(1.0, s))
    l = max(0.0, min(1.0, l))

    # From FvD 13.37, CSS Color Module Level 3
    if l <= .5:
        m2 = l * (1 + s)
    else:
        m2 = l + s - l * s
    m1 = 2 * l - m2

    def v(h):
        if (h > 360):
            h -= 360
        elif (h < 0):
            h += 360
        if (h < 60):
            return m1 + (m2 - m1) * h / 60
        if (h < 180):
            return m2
        if (h < 240):
            return m1 + (m2 - m1) * (240 - h) / 60
        return m1

    def vv(h):
        return round(v(h) * 255)

    return (int(vv(h + 120)), int(vv(h)), int(vv(h - 120)))


parser = argparse.ArgumentParser(description='Calculate an image gradient.')
parser.add_argument('input', help='an image path as input')
parser.add_argument('--format', default="hslimage", help='save output format, default hslimage (direction is hue, magnitude is lightness). Other options: magimage (magnitude image), json.')
parser.add_argument('--output', help='save to output path')
parser.add_argument('--width', type=int, default=None, help='resize width, default: None (no resize)')
parser.add_argument('--svg', default=None, help='output svg path')
parser.add_argument('--svg-grid-size', type=int, default=25, help='density of svg arrows (default: 25)')
parser.add_argument('--svg-mag-scale', type=float, default=5.0, help='svg arrow magnitude scaler (default: 5.0)')
args = parser.parse_args()

# Based on Programming Computer Vision, chapter 1, Image Derivatives

p = args.input
path, base = os.path.split(p)
base, ext = os.path.splitext(base)
out = args.output # or os.path.join(path, base + ".gradient.png")

im = Image.open(p).convert('L')
width, height = im.size
if args.width:
    scaled_height = int(height * (float(args.width)/width))
    im.thumbnail((args.width, scaled_height))
    width, height = args.width, scaled_height
im = array(im)

# Sobel derivative filters
# imx = zeros(im.shape)
# filters.sobel(im,1,imx)
# imy = zeros(im.shape)
# filters.sobel(im,0,imy)
# magnitude = sqrt(imx**2+imy**2)

sigma = 5 # standard deviation
imx = zeros(im.shape)
filters.gaussian_filter(im, (sigma,sigma), (0,1), imx)
imy = zeros(im.shape)
filters.gaussian_filter(im, (sigma,sigma), (1,0), imy)
magnitude = sqrt(imx**2+imy**2)
dirs = arctan2(imy, imx)

# Save as image
# pil_im = Image.fromarray(dirs)
# pil_im.convert("LA").save("dirs.png")


totalmag = 0.0

if out:
    if args.format == "hslimage":
        # Map direction to Hue, Magnitude to value
        from math import pi
        maxmag = amax(magnitude)
        # height, width = magnitude.shape
        im = Image.new("RGBA", (width, height))
        # draw = ImageDraw.ImageDraw(im)
        for y in range(height):
            p = int(math.ceil(float(y)/height*100))
            sys.stderr.write("\rCreating gradient HSL image... [{0}%]".format(p))
            sys.stderr.flush()
            for x in range(width):
                d = dirs[y][x]
                hue = ((d+pi) / (2 * pi)) * 360
                m = magnitude[y][x]
                value = (m/maxmag)
                r, g, b = hsl_to_rgb(hue, 1.0, value)
                im.putpixel((x, y), (r, g, b, 255))
                if args.svg and (x % args.svg_grid_size == 0) and (y % args.svg_grid_size == 0):
                    x1, y1 = x, y
                    x2 = x1 + (m * args.svg_mag_scale * math.cos(d))
                    y2 = y1 + (m * args.svg_mag_scale * math.sin(d))
                    print ("""<path
           style="fill:none;stroke:#00FFFF;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;marker-end:url(#Arrow1Send)"
           d="M {0},{1} {2},{3}"
           id="path2985"/>""".format(x1, y1, x2, y2), file=svgfile)
                    totalmag += m

                # draw.point((x, y), fill=(r,g,b,255))
        sys.stderr.write("\n")
        im.save(out)
    elif args.format == "magimage":
        pil_im = Image.fromarray(magnitude)
        pil_im.convert("LA").save(out)
    elif args.format == "json":
        with open(out, "w") as f:
            json.dump({
                'dirs': dirs.tolist(),
                'magnitudes': magnitude.tolist()
            }, f)

if args.svg:
    svgfile = open(args.svg, 'w')
    print("""<svg version="1.1" baseProfile="full" width="{0[width]}" height="{0[height]}" xmlns="http://www.w3.org/2000/svg">
  <defs
     id="defs4">
    <marker
       orient="auto"
       refY="0.0"
       refX="0.0"
       id="Arrow1Send"
       style="overflow:visible;">
      <path
         id="path3774"
         d="M 0.0,0.0 L 5.0,-5.0 L -12.5,0.0 L 5.0,5.0 L 0.0,0.0 z "
         style="fill-rule:evenodd;stroke:#00FFFF;fill:#00FFFF;stroke-width:1.0pt;"
         transform="scale(0.2) rotate(180) translate(6,0)" />
    </marker>
  </defs>
  <g
     id="layer1">""".format({'width': width, 'height': height}), file=svgfile)

    from math import pi
    maxmag = amax(magnitude)
    # height, width = magnitude.shape
    im = Image.new("RGBA", (width, height))
    # draw = ImageDraw.ImageDraw(im)
    for y in range(0, height, args.svg_grid_size):
        for x in range(0, width, args.svg_grid_size):
            d = dirs[y][x]
            hue = ((d+pi) / (2 * pi)) * 360
            m = magnitude[y][x]
            x1, y1 = x, y
            x2 = x1 + (m * args.svg_mag_scale * math.cos(d))
            y2 = y1 + (m * args.svg_mag_scale * math.sin(d))
            print ("""<path
   style="fill:none;stroke:#00FFFF;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;marker-end:url(#Arrow1Send)"
   d="M {0},{1} {2},{3}"
   id="path2985"/>""".format(x1, y1, x2, y2), file=svgfile)

    print ("""</g>
</svg>""", file=svgfile)
    svgfile.close()
    print (int(totalmag), file=sys.stdout)

# with open(os.path.join(path, base + ".gradient_mag.json"), "w") as f:


