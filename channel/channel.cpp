#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

using namespace cv;
using namespace std;

// Mat src; Mat src_gray;
// int thresh = 40;
// int max_thresh = 305;


/** @function main */
int main( int argc, char** argv )
{
    if (argc < 5) {
        fprintf(stderr, "usage: channel IMAGE RED GREEN BLUE\n");
        return (1);
    }
    // char outname[255];
	Mat src = imread(argv[1], CV_LOAD_IMAGE_COLOR); //load  image
	Mat bgr[3];   //destination array
	// printf("Size of input image %d, %d\n", src.rows, src.cols);
	Mat jimg( src.rows, src.cols, CV_8UC4, Scalar(0,0,0,0) ); // joined image (output)
	Mat dst, tdst;

	split(src, bgr);//split source

	int zero_to_zero[] = { 0,0 };
	int one_to_zero[] = { 1,0 };
	int zero_to_one[] = { 0,1 };
	int zero_to_two[] = { 0,2 };
	int zero_to_three[] = {0, 3};
	int one_to_three[] = {1, 3};

	// blue - (green+red)/2
	addWeighted(bgr[0], 1.0, bgr[1], -0.5, 0.0, dst);
	addWeighted(dst, 1.0, bgr[2], -0.5, 0.0, dst);
	threshold(dst, tdst, 16, 255, THRESH_BINARY);
	mixChannels(&tdst, 1, &jimg, 4, zero_to_zero, 1);
	// use same values as ALPHA
	mixChannels(&tdst, 1, &jimg, 4, zero_to_three, 1);
	imwrite(argv[4], jimg);
	// black out the blue channel to continue
	mixChannels(&jimg, 4, &jimg, 4, one_to_zero, 1);
	mixChannels(&jimg, 4, &jimg, 4, one_to_three, 1);

	// green - (blue + red)/2
	addWeighted(bgr[1], 1.0, bgr[0], -0.5, 0.0, dst);
	addWeighted(dst, 1.0, bgr[2], -0.5, 0.0, dst);
	threshold(dst, tdst, 16, 255, THRESH_BINARY);
	mixChannels(&tdst, 1, &jimg, 4, zero_to_one, 1);
	mixChannels(&tdst, 1, &jimg, 4, zero_to_three, 1);
	imwrite(argv[3], jimg);
	// black out the green channel to continue
	mixChannels(&jimg, 4, &jimg, 4, zero_to_one, 1);
	mixChannels(&jimg, 4, &jimg, 4, zero_to_three, 1);

	addWeighted(bgr[2], 1.0, bgr[0], -0.5, 0.0, dst);
	addWeighted(dst, 1.0, bgr[1], -0.5, 0.0, dst);
	threshold(dst, tdst, 16, 255, THRESH_BINARY);
	mixChannels(&tdst, 1, &jimg, 4, zero_to_two, 1);
	mixChannels(&tdst, 1, &jimg, 4, zero_to_three, 1);
	imwrite(argv[2], jimg);

}
