Channels
===========

Separating red, green, and blue channels using Open CV and C++.

Still todo:

* thresholding
* alpha in output (to only show color)


* [matrix ops in opencv](http://docs.opencv.org/2.4/modules/core/doc/operations_on_arrays.html)

```cpp
int main( int argc, char** argv )
{
    if (argc < 2) {
        fprintf(stderr, "usage: channel IMAGE red.png green.png blue.png\n");
        return (0);
    }
	Mat src = imread(argv[1], CV_LOAD_IMAGE_COLOR); //load  image
	Mat bgr[3];   //destination array
	split(src,bgr);//split source
	//Note: OpenCV uses BGR color order
	imwrite("blue.png",bgr[0]); //blue channel
	imwrite("green.png",bgr[1]); //green channel
	imwrite("red.png",bgr[2]); //red channel
}
```

<style>
div.figure {
	float: left;
	margin-right: 1em;
}
</style>

Using *addWeighted* to subtract other two layers. 

```cpp
int main( int argc, char** argv )
{
    if (argc < 2) {
        fprintf(stderr, "usage: channel IMAGE red.png green.png blue.png\n");
        return (0);
    }
    char outname[255];
	Mat src = imread(argv[1], CV_LOAD_IMAGE_COLOR); //load  image
	Mat bgr[3];   //destination array

	split(src,bgr);//split source
	//Note: OpenCV uses BGR color order
	
	Mat dst;

	addWeighted(bgr[0], 1.0, bgr[1], -0.5, 0.0, dst);
	addWeighted(dst, 1.0, bgr[2], -0.5, 0.0, dst);
	sprintf(outname, "%s.bluex.png", argv[1]);
	imwrite(outname, dst);

	addWeighted(bgr[1], 1.0, bgr[0], -0.5, 0.0, dst);
	addWeighted(dst, 1.0, bgr[2], -0.5, 0.0, dst);
	sprintf(outname, "%s.greenx.png", argv[1]);
	imwrite(outname, dst);

	addWeighted(bgr[2], 1.0, bgr[0], -0.5, 0.0, dst);
	addWeighted(dst, 1.0, bgr[1], -0.5, 0.0, dst);
	sprintf(outname, "%s.redx.png", argv[1]);
	imwrite(outname, dst);

}
```

### Results

Test image, simple channel separation and subtracted layers:

![Original graphic](sample.png)

![blue](sample.png.blue.png)

![green](sample.png.green.png)

![red](sample.png.red.png)

<div style="clear: both"></div>

![Original graphic](sample.png)

![blue - (green+red)/2](sample.png.bluex.png)

![green - (red+blue)/2](sample.png.greenx.png)

![red - (blue+green)/2](sample.png.redx.png)

<div style="clear: both"></div>

Now on a photographic image:

![Original photograph](220px-Donna_Haraway_and_Cayenne.jpg)

![blue](220px-Donna_Haraway_and_Cayenne.jpg.blue.png)

![green](220px-Donna_Haraway_and_Cayenne.jpg.green.png)

![red](220px-Donna_Haraway_and_Cayenne.jpg.red.png)

<div style="clear: both"></div>

![Original photograph](220px-Donna_Haraway_and_Cayenne.jpg)

![blue - (green+red)/2](220px-Donna_Haraway_and_Cayenne.jpg.bluex.png)

![green - (red+blue)/2](220px-Donna_Haraway_and_Cayenne.jpg.greenx.png)

![red - (blue+green)/2](220px-Donna_Haraway_and_Cayenne.jpg.redx.png)

<div style="clear: both"></div>

Now I want to show these split channels back in their original color, so I want to join them back into a normal image matrix with three (or four) color channels. According to the [operations on arrays documentation](http://docs.opencv.org/2.4/modules/core/doc/operations_on_arrays.html#void%20mixChannels%28const%20Mat*%20src,%20size_t%20nsrcs,%20Mat*%20dst,%20size_t%20ndsts,%20const%20int*%20fromTo,%20size_t%20npairs%29), the function that would seem to do this is *mixChannels*.

```cpp
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

using namespace cv;
using namespace std;

// Mat src; Mat src_gray;
// int thresh = 40;
// int max_thresh = 305;


/** @function main */
int main( int argc, char** argv )
{
    if (argc < 2) {
        fprintf(stderr, "usage: channel IMAGE red.png green.png blue.png\n");
        return (0);
    }
    char outname[255];
	Mat src = imread(argv[1], CV_LOAD_IMAGE_COLOR); //load  image
	Mat bgr[3];   //destination array

	split(src,bgr);//split source
	//Note: OpenCV uses BGR color order
	

	Mat dst;

	addWeighted(bgr[0], 1.0, bgr[1], -0.5, 0.0, dst);
	addWeighted(dst, 1.0, bgr[2], -0.5, 0.0, dst);
	sprintf(outname, "%s.bluex.png", argv[1]);
	imwrite(outname, dst);

	printf("Size of input image %d, %d\n", src.rows, src.cols);
	Mat jimg( src.rows, src.cols, CV_8UC3, Scalar(0,0,0) );
	int map_to_blue[] = { 0,0 };
	int map_to_green[] = { 0,1 };
	int map_to_red[] = { 0,2 };
	mixChannels(&dst, 1, &jimg, 3, map_to_blue, 1);

	addWeighted(bgr[1], 1.0, bgr[0], -0.5, 0.0, dst);
	addWeighted(dst, 1.0, bgr[2], -0.5, 0.0, dst);
	sprintf(outname, "%s.greenx.png", argv[1]);
	imwrite(outname, dst);

	mixChannels(&dst, 1, &jimg, 3, map_to_green, 1);

	addWeighted(bgr[2], 1.0, bgr[0], -0.5, 0.0, dst);
	addWeighted(dst, 1.0, bgr[1], -0.5, 0.0, dst);
	sprintf(outname, "%s.redx.png", argv[1]);
	imwrite(outname, dst);
	mixChannels(&dst, 1, &jimg, 3, map_to_red, 1);
	imwrite("channels.png", jimg);

}
```

![Original photograph](220px-Donna_Haraway_and_Cayenne.jpg)

![channels](channels.png)

<div style="clear: both"></div>

Version with thresholding and separate output with alpha (like the old python script).

```cpp
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

using namespace cv;
using namespace std;

// Mat src; Mat src_gray;
// int thresh = 40;
// int max_thresh = 305;


/** @function main */
int main( int argc, char** argv )
{
    if (argc < 5) {
        fprintf(stderr, "usage: channel IMAGE RED GREEN BLUE\n");
        return (1);
    }
    // char outname[255];
	Mat src = imread(argv[1], CV_LOAD_IMAGE_COLOR); //load  image
	Mat bgr[3];   //destination array
	// printf("Size of input image %d, %d\n", src.rows, src.cols);
	Mat jimg( src.rows, src.cols, CV_8UC4, Scalar(0,0,0,0) ); // joined image (output)
	Mat dst, tdst;

	split(src, bgr);//split source

	int zero_to_zero[] = { 0,0 };
	int one_to_zero[] = { 1,0 };
	int zero_to_one[] = { 0,1 };
	int zero_to_two[] = { 0,2 };
	int zero_to_three[] = {0, 3};
	int one_to_three[] = {1, 3};

	// blue - (green+red)/2
	addWeighted(bgr[0], 1.0, bgr[1], -0.5, 0.0, dst);
	addWeighted(dst, 1.0, bgr[2], -0.5, 0.0, dst);
	threshold(dst, tdst, 16, 255, THRESH_BINARY);
	mixChannels(&tdst, 1, &jimg, 4, zero_to_zero, 1);
	// use same values as ALPHA
	mixChannels(&tdst, 1, &jimg, 4, zero_to_three, 1);
	imwrite(argv[4], jimg);
	// black out the blue channel to continue
	mixChannels(&jimg, 4, &jimg, 4, one_to_zero, 1);
	mixChannels(&jimg, 4, &jimg, 4, one_to_three, 1);

	// green - (blue + red)/2
	addWeighted(bgr[1], 1.0, bgr[0], -0.5, 0.0, dst);
	addWeighted(dst, 1.0, bgr[2], -0.5, 0.0, dst);
	threshold(dst, tdst, 16, 255, THRESH_BINARY);
	mixChannels(&tdst, 1, &jimg, 4, zero_to_one, 1);
	mixChannels(&tdst, 1, &jimg, 4, zero_to_three, 1);
	imwrite(argv[3], jimg);
	// black out the green channel to continue
	mixChannels(&jimg, 4, &jimg, 4, zero_to_one, 1);
	mixChannels(&jimg, 4, &jimg, 4, zero_to_three, 1);

	addWeighted(bgr[2], 1.0, bgr[0], -0.5, 0.0, dst);
	addWeighted(dst, 1.0, bgr[1], -0.5, 0.0, dst);
	threshold(dst, tdst, 16, 255, THRESH_BINARY);
	mixChannels(&tdst, 1, &jimg, 4, zero_to_two, 1);
	mixChannels(&tdst, 1, &jimg, 4, zero_to_three, 1);
	imwrite(argv[2], jimg);

}
```

<div style="clear: both"></div>

![Original photograph](220px-Donna_Haraway_and_Cayenne.jpg)

![blue - (green+red)/2](donna.blue.png)

![green - (red+blue)/2](donna.green.png)

![red - (blue+green)/2](donna.red.png)
