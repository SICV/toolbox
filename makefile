
all: README.html


################
# RULES TO MAKE THE TOOLS

bin = bin/channel bin/contours bin/houghlines bin/texture

bin: $(bin)

bin/channel:
	$(MAKE) -C channel all
	mkdir -p bin
	cp channel/channel bin

bin/contours:
	$(MAKE) -C contours all
	mkdir -p bin
	cp contours/contours bin

bin/houghlines:
	mkdir -p houghlines/build
	cd houghlines/build && cmake .. && make
	mkdir -p bin
	cp houghlines/build/houghlines bin

bin/texture:
	mkdir -p texture/build
	cd texture/build && cmake .. && make
	mkdir -p bin
	cp texture/build/texture bin

########################

# markdown to HTML (for docs)
%.html: %.md
	pandoc --self-contained $< -o $@


#################################
# RULES to USE the tools

# contours
%.contours.svg: %.png
	bin/contours $< > $@
%.contours.svg: %.jpg
	bin/contours $< > $@

# texture (& lexicality, textline, para, block
%.texture.svg: %.png
	bin/texture $< $*.texture.svg $*.lexicality.svg $*.textline.svg $*.para.svg $*.block.svg
%.texture.svg: %.jpg
	bin/texture $< $*.texture.svg $*.lexicality.svg $*.textline.svg $*.para.svg $*.block.svg

# channel
%.red.png: %.png
	bin/channel $< $*.red.png $*.green.png $*.blue.png
%.red.png: %.jpg
	bin/channel $< $*.red.png $*.green.png $*.blue.png
%.green.png: %.png
	bin/channel $< $*.red.png $*.green.png $*.blue.png
%.green.png: %.jpg
	bin/channel $< $*.red.png $*.green.png $*.blue.png
%.blue.png: %.png
	bin/channel $< $*.red.png $*.green.png $*.blue.png
%.blue.png: %.jpg
	bin/channel $< $*.red.png $*.green.png $*.blue.png

# houghlines
# Z3.hough.png <= Z3.png
# Z3.hough.X0Y0.png <= Z3.hough.png
%.hough.svg: %.jpg
	bin/houghlines $< > $@
%.hough.svg: %.png
	bin/houghlines $< > $@

# gradient: NEEDS WORK
# %.gradient.png: %.jpg
# 	python imagegradient/imagegradient.py --output $*.gradient.png --svg $*.gradient.svg $<
%.gradient.svg: %.jpg
	python imagegradient/imagegradient.py --svg $*.gradient.svg $< 
# %.gradient.png: %.png
# 	python imagegradient/imagegradient.py --output $*.gradient.png --svg $*.gradient.svg $<
%.gradient.svg: %.png
	python imagegradient/imagegradient.py --svg $*.gradient.svg $<

################################
# VIEWER rules
# pyramid rules

TILESIZE=128

.PRECIOUS: %/Z0.png %/Z1.png %/Z2.png %/Z3.png %/Z4.png %/Z5.png
%/Z0.png: %.png
	mkdir -p $*
	python viewer/pyramid.py $< --tilesize $(TILESIZE) --output "$*/Z{z}.png"

%/Z0.png: %.jpg
	mkdir -p $*
	python viewer/pyramid.py $< --tilesize $(TILESIZE) --output "$*/Z{z}.png"

# SVG to png
%.png: %.svg
	inkscape --export-background-opacity=0.0 --export-png=$@ $<

# tile rule
%.X0Y0.png : %.png
	python viewer/tile.py $< --tilesize $(TILESIZE) --output "$*.X{x}Y{y}.png"

# make the map
%.html: %.png
	mkdir -p $*
	python viewer/makeviewer.py \
		--tilesize $(TILESIZE) \
		--template viewer/map.html \
		--output $@ $<
%.html: %.jpg
	mkdir -p $*
	python viewer/makeviewer.py \
		--tilesize $(TILESIZE) \
		--template viewer/map.html \
		--output $@ $<

%/error.jpg:
	convert -size $(TILESIZE)x$(TILESIZE) canvas:red $@
