#!/usr/bin/env python
from __future__ import print_function
import cv2, os, json, sys
from argparse import ArgumentParser
from analyze_image import analyze_image

p = ArgumentParser("")
p.add_argument("--cascades", default="./haarcascades", help="location of the cascade XML files, default: ./haarcascades")
p.add_argument("--settings", default="settings.json", help="settings file, default settings.json")

p.add_argument("--resize", default=False, action="store_true")
p.add_argument("--width", type=int, default=640, help="pre-detect resize width")
p.add_argument("--height", type=int, default=480, help="pre-detect resize height")
p.add_argument("--background", default="255,255,255")
p.add_argument("--pad", default=False, action="store_true")

p.add_argument("input", nargs="*", default=[])
args = p.parse_args()

background = tuple([int(x) for x in args.background.split(",")])

def resize (img, w, h, interpolation = cv2.INTER_CUBIC):
    ih, iw, ic = img.shape
    if (ih > h) or (iw > w):
        # try fitting width
        sw = w
        sh = int(sw * (float(ih)/iw))
        if sh > h:
            # fit height instead
            sh = h
            sw = int(sh * (float(iw)/ih))
        return cv2.resize(img, (sw, sh), interpolation=interpolation)
    return img

def pad (img, w, h, color=(0, 0, 0)):
    ih, iw, ic = img.shape
    top = (h - ih) / 2
    bottom = h - ih - top
    left = (w - iw) / 2
    right = (w - iw - left)
    return cv2.copyMakeBorder(img, top, bottom, left, right, cv2.BORDER_CONSTANT, value=color)

with open (args.settings) as f:
    settings = json.load(f)

cascades_path = os.path.expanduser(args.cascades)

def init_features (f):
    if type(f) == list:
        return [init_features(x) for x in f]
    f.get("cascade")
    f['_cascade'] = cv2.CascadeClassifier(os.path.join(cascades_path, f["cascade"]))
    if 'args' not in f:
        f['args'] = {}
    return f

def ensure_list (f):
    if type(f) == list:
        return f
    else:
        return [f]

def tuple_lists(args):
    for (key, value) in args.items():
        if type(value) == list:
            args[key] = tuple(value)
    return args

def process_features (grayimg, features):
    ret = {}
    for x in features:
        roi = grayimg
        if type(x) == list:
            f1, f2 = x
        else:
            f1 = x
            f2 = None


        if f1['name'] not in ret:
            rects = ret[f1['name']] = []
        else:
            rects = ret[f1['name']]
        if f2:
            if f2['name'] not in ret:
                rects2 = ret[f2['name']] = []
            else:
                rects2 = ret[f2['name']]

        for (x,y,w,h) in f1['_cascade'].detectMultiScale(roi, **tuple_lists(f1['args'])):
            rects.append([int(x), int(y), int(w), int(h)])
            # narrow for (evt) next cascaded feature...
            if f2:
                roi = roi[y:y+h, x:x+w]
                for (x2,y2,w2,h2) in f2['_cascade'].detectMultiScale(roi, **tuple_lists(f2['args'])):
                    rects2.append([int(x+x2), int(y+y2), int(w2), int(h2)])
    return ret

output = []
inputs = args.input
numinput = len(inputs)
if (numinput == 0):
    # attempt to read filenames from stdin
    inputs = sys.stdin.read().splitlines()
    inputs = [x.strip() for x in inputs if x.strip()]

numinput = len(inputs)
features = init_features(settings['features'])
for i, path in enumerate(inputs):
    print ("{0}/{1} {2}".format(i, numinput, path), file=sys.stderr)
    img = cv2.imread(path)

    if args.resize:
        img = resize(img, args.width, args.height)
    if args.pad:
        img = pad(img, args.width, args.height, background)

    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    item_features = process_features(gray, features)
    item = {"path": path, "features": item_features}
    output.append(item)
print (json.dumps(output, indent=2))
