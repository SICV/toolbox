$(function(){
    var ww = $(window).width(),
        wh = $(window).height(),
        image=document.createElement("img"),
        canvas, ctx;

    image.onload=function() {
        canvas = $("<canvas></canvas>")
            .attr("width", image.width)
            .attr("height", image.height)
            // .css("left", ((ww-image.width)/2) + "px")
            .appendTo("#canvas")
            .get(0);
        // console.log("image size", image.width, image.height);
        ctx=canvas.getContext("2d");
        // ctx.scale(5.0, 5.0);
        // ctx.translate((ww-image.width)/2, 0);
        ctx.drawImage(image,0,0);

        ctx.strokeStyle= 'rgba(0,255,0,0.66)';
        ctx.lineWidth = 6;
        // ctx.translate(-200, -200);
	// ok this should actually be checked against the actual size of the "540" image...
	// they all appear to be made x540
        var scaler = image.height / 540;
        ctx.scale(scaler, scaler);
        start();

        // draw();
        // ctx.drawImage(image,canvas.width/2-image.width/2,canvas.height/2-image.width/2);
    }
    image.src=image_source;

    var shapes = [],
        current_shape,
        current_point,
        shape_index = 0,
        point_index = 0,
        done = false;

    function randcolor () { return Math.round(Math.random()*255); }
    function randStrokeStyle () { return 'rgba('+randcolor()+','+randcolor()+','+randcolor()+',0.50)'; }
    function zero () { return 0; }
    function randSpeed () { return -1 + Math.random()*2; }

    var shapeProps = [];
    function getShapeProperty(i, name, setter) {
        var props = shapeProps[i],
            ret;
        if (props == undefined) {
            props = {}
            shapeProps[i] = props;
        }
        ret = props[name];
        if (ret == undefined) {
            ret = setter();
            props[name] = ret;
        }
        return ret;
    }

    function drawAllShapes () {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        for (var si=0; si<shapes.length; si++) {
            var shape = shapes[si];
            ctx.beginPath();
            ctx.strokeStyle = getShapeProperty(si, 'strokeStyle', randStrokeStyle);
            var dx = getShapeProperty(si, 'dx', zero);
            var dy = getShapeProperty(si, 'dy', zero);
            // var dr = getShapeProperty(si, 'dr', zero);

            var sx = getShapeProperty(si, 'sx', randSpeed);
            var sy = getShapeProperty(si, 'sy', randSpeed);
            // var sr = getShapeProperty(si, 'sr', randRot);

            ctx.translate(dx, dy);
            // ctx.rotate(dr);
            ctx.strokeStyle = getShapeProperty(si, 'strokeStyle', randStrokeStyle);
            for (var pi=0; pi<shape.length; pi++) {
                if (pi == 0) {
                    ctx.moveTo(shape[pi].x, shape[pi].y);
                } else {
                    ctx.lineTo(shape[pi].x, shape[pi].y);
                }
            }
            ctx.stroke();
            // console.log(sx, sy);
            shapeProps[si].dx += sx; //-1 + Math.rand()*2;
            shapeProps[si].dy += sy; //-1 + Math.rand()*2;
            // shapeProps[si].dr += sr; //-1 + Math.rand()*2;
            // ctx.rotate(-dr);
            ctx.translate(-dx, -dy);
        }
    }

  var interval = function () {
    // console.log("interval", shape_index, point_index);
    var keepGoing = false, endOfShape = false;
    for (var loop_counter=0; loop_counter < 3; loop_counter++) {
        var current_shape = shapes[shape_index];
        ctx.strokeStyle = getShapeProperty(shape_index, 'strokeStyle', randStrokeStyle);
            
        if (current_shape && point_index > 0 && point_index < current_shape.length) {
            // console.log("drawing shape:", shape_index, ", point:", point_index-1, "-", point_index);
            ctx.beginPath();
            ctx.moveTo(current_shape[point_index-1].x, current_shape[point_index-1].y);
            ctx.lineTo(current_shape[point_index].x, current_shape[point_index].y);
            ctx.stroke();
        }
        if (current_shape && ++point_index >= current_shape.length) {
            // console.log("END OF SHAPE");
            // randStrokeStyle.strokeStyle= 'rgba('+randcolor()+','+randcolor()+','+randcolor()+',0.50)';
            if (++shape_index < shapes.length) {
                point_index = 0;
                // window.setTimeout(interval, 1000);
                keepGoing = true;
                endOfShape = true;
            } else {
                window.setTimeout(function () {
                    // console.log("contour draw is done", $("body"));
                    doneDrawing();
                }, 2000);
            }
        } else {
            // window.setTimeout(interval, 10);
            keepGoing = true;
        }
        if (!keepGoing) break;
    }
    if (keepGoing) {
        window.setTimeout(interval, endOfShape ? 1 : 1);
    }

  };
  function start () {
    $.ajax(json_source, {
        dataType: "json",
        success: function (data) {
            // console.log("contours", data);
            shapes = data;
            if (shapes.length == 0) {
                doneDrawing();
            } else {
                interval();
            }
        },
        error: function (e) {
            console.log("error", e);
            doneDrawing();
            // parent.postMessage("done", "*");
        }
    });
  }

  function doneDrawing () {
    // parent.postMessage("done", "*");
    window.setTimeout(function () {
        drawAllShapes();
        // window.setInterval(drawAllShapes, 10);
        window.setTimeout(function () {
            parent.postMessage("done", "*");
        }, 5000)
    }, 5000);
  }

// SHAPE0 P P P
// SHAPE1 P P

}); // end $(function(){});
