<!DOCTYPE html>
<?php

$src = $_REQUEST['src'];
if (!isset($src)) { $src = "/contours/S-20-1-11.jpg"; }
$datasrc = preg_replace('/_\d+.jpg$/i', "_540.json", $src);

?>
<html>
<head>
    <meta charset="utf-8" />
    <script type="text/javascript" src="/lib/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="contourdraw.css">
    <script>
        var image_source = "<?php echo $src ?>",
         json_source = "<?php echo $datasrc ?>",
         json_scale = 2.0;
    </script>
    <script src="contourdraw2.js"></script>
</head>

<body>
    <div id="canvas"></div>
</body>

</html>