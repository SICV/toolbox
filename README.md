```
   __              ____              
  / /_____  ____  / / /_  ____  _  __
 / __/ __ \/ __ \/ / __ \/ __ \| |/_/
/ /_/ /_/ / /_/ / / /_/ / /_/ />  <  
\__/\____/\____/_/_.___/\____/_/|_|  
```
your personal image processing toolset!


Installing / Building
-------------------------

This toolset is heterogeneous; some are written in C with bindings to libraries like opencv and tesseract, others are in the python scripting language and rely on python packages like numpy, python-opencv, pillow, and scipy.


### Python

For the python tools, it may be useful to create a "virtualenv". Otherwise you might eventually have trouble with version conflicts with other tools. Option A describes this approach. Alternatively, you can install the packages in your system (option B).


#### A. Installing Python dependencies in a venv

1. Create one:

	virtualenv venv

2. Activate it:

	source venv/bin/activate

3. Use pip to install (nb you don't need sudo since you "own" the venv).

	pip install scipy pillow numpy


#### B. Installing Python dependencies to the system

	sudo pip install scipy pillow numpy

### C

Some of the C-based tools like *contours* and *channel* have a simple makefile and build simply with:

```bash
cd contours
make
```

Other tools like *texture* and *houghlines* use cmake. Basically, if there is a file called "CMakeLists.txt" the build process uses cmake. An example of building texture would be:

```bash
cd texture
mkdir build
cd build
cmake ..
make
```


Usage
-----------

### Image Gradient

	python python/imagegradient.py foo.png --output foo.gradient.png --svg foo.gradient.svg

Converts foo.png (input) into two outputs.


Notes
-------------
* Viewer: Improve layers to show layers loading / loaded status (useful when browsing via cookbook)
* Viewer: Add SVG layer support (technically possible ?! via img tag)... yes! using iframe ipv img
* Viewer: Layer modes (such as masking ?!) ... and eventually export to other formats (SVG with image layers!)
* Viewer: ?! Represent entire stacking as an SVG (to begin with!!!)

### Nov 24 2016

Incorporate orderings functions:
* Way to talk of "items" and collect metadata in a single JSON.
* Scripts to generate (leaflet) maps based on metadata.
* Cross linkages with other visualisations (such as the map)

What's more complex though is that now each layer is based on a resolution which may (or may not) change the resulting order. So the orderings are now PER ZOOM LEVEL (which is interesting).

ALSO need to make a scraper for RMAH -- do it *promiscuously*?!

