#!/usr/bin/env python

from __future__ import print_function
from argparse import ArgumentParser
from PIL import Image
from math import ceil
import os, sys, re


p = ArgumentParser("tile an image")
p.add_argument("--tilesize", type=int, default=256, help="default: 256")
p.add_argument("input")
p.add_argument("--output", default="./{base}.Z{z}X{x}Y{y}.png", 
	help="output template, default: ./{base}.Z{z}X{x}Y{y}.png")
p.add_argument("--maxzoom", type=int, default=5, help="default 5")
p.add_argument("--minzoom", type=int, default=1, help="default 1")
args = p.parse_args()


def format_pre_process (x):
	# {x} => {0[x]}
	return re.sub(r"\{(.+?)\}", r"{0[\1]}", x)

def tile(im, z):
	w, h = im.size
	cols = int(ceil(float(w) / args.tilesize))
	rows = int(ceil(float(h) / args.tilesize))
	for y in range(rows):
	 	for x in range(cols):
	 		bx = x * args.tilesize
	 		by = y * args.tilesize
	 		bx2 = bx + args.tilesize
	 		by2 = by + args.tilesize
	 		cim = im.crop((bx, by, bx2, by2))
	 		op = output.format({'base': base, 'z':z, 'x':x, 'y':y})
	 		print (op)
	 		cim.save(op)

output= format_pre_process(args.output)
im = Image.open(args.input).convert("RGBA")
d, f = os.path.split(args.input)
base, ext = os.path.splitext(f)
w, h = im.size

scale = 0
for z in range(args.maxzoom, args.minzoom-1, -1):
	print ("z", z, file=sys.stderr)
	if scale == 0:
		useim = im
	else:
		sf = 2**scale
		sw = int(w / sf)
		sh = int(h / sf)
		# if scale < 1:
		# 	useim = im.resize((sw,sh), resample=Image.NEAREST)
		# else:
		useim = im.resize((sw,sh), resample=Image.ANTIALIAS)

	tile(useim, z)
	scale += 1



