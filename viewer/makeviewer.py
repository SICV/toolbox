#!/usr/bin/env python
from __future__ import print_function
from jinja2 import Environment, FileSystemLoader
import os
from argparse import ArgumentParser
from PIL import Image
from pyramid import check_height


ap = ArgumentParser("")
ap.add_argument("input")
ap.add_argument("--template", default="viewer/viewer.html")
ap.add_argument("--output", default=None)
ap.add_argument("--tilesize", type=int, default=256, help="default: 256")
args = ap.parse_args()


outpath = None
original = args.input
if args.output:
	outpath, _ = os.path.split(args.output)
	original = os.path.relpath(original, outpath)

basename, _ = os.path.splitext(original)

env = Environment(loader=FileSystemLoader("."))
t = env.get_template(args.template)
tvars = {}
tvars['original'] = original
tvars['basename'] = basename

im = Image.open(args.input)
tvars['width'] = im.size[0]
tvars['height'] = im.size[1]
maxzoom, sw, sh = check_height(im, args.tilesize)
tvars['maxzoom'] = maxzoom
tvars['tilesize'] = args.tilesize

if args.output:
	with open(args.output, "w") as f:
		print (t.render(**tvars).encode("utf-8"), file=f)
else:
	print (t.render(**tvars).encode("utf-8"))
