#!/usr/bin/env python

from __future__ import print_function
from argparse import ArgumentParser
from PIL import Image
from math import ceil
import os, sys, re


p = ArgumentParser("tile an image (single layer)")
p.add_argument("input")
p.add_argument("--tilesize", type=int, default=256, help="default: 256")
p.add_argument("--output", default="./{base}.X{x}Y{y}.png", help="output template, default: ./{base}.X{x}Y{y}.png")
args = p.parse_args()


def format_preprocess (x):
	# {x} => {0[x]}
	return re.sub(r"\{(.+?)\}", r"{0[\1]}", x)

def tile(im, output, base, tilesize=256):
	w, h = im.size
	cols = int(ceil(float(w) / tilesize))
	rows = int(ceil(float(h) / tilesize))
	for y in range(rows):
	 	for x in range(cols):
	 		bx = x * tilesize
	 		by = y * tilesize
	 		bx2 = bx + tilesize
	 		by2 = by + tilesize
	 		cim = im.crop((bx, by, bx2, by2))
	 		op = output.format({'base': base, 'x': x, 'y':y})
	 		print (op)
	 		cim.save(op)

if __name__ == "__main__":
	output = format_preprocess(args.output)
	im = Image.open(args.input).convert("RGBA")
	d, f = os.path.split(args.input)
	base, ext = os.path.splitext(f)
	# w, h = im.size
	tile(im, output, base, tilesize=args.tilesize)




