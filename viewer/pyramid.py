#!/usr/bin/env python

from __future__ import print_function
from argparse import ArgumentParser
from PIL import Image
from math import ceil
import os, sys, re


def format_preprocess (x):
    # {x} => {0[x]}
    return re.sub(r"\{(.+?)\}", r"{0[\1]}", x)

def check_height (im, tilesize=256):
    w, h = im.size
    scale = 0
    sw, sh = w, h
    while sw > tilesize:
        scale += 1
        sf = 2**scale
        sw = int(w / sf)
        sh = int(h / sf)
    return (scale, sw, sh)

def pyramid (im, output, tilesize=256):
    w, h = im.size
    scale, sw, sh = check_height(im, tilesize)
    for s in range(scale, -1, -1):
        z = scale-s
        sf = 2**s
        sw = int(w / sf)
        sh = int(h / sf)
        outname = output.format({'z': z})
        if s == 0:
            outim = im
        else:
            outim = im.resize((sw,sh), resample=Image.ANTIALIAS)
        print (outname, sw, sh)
        outim.save(outname)
    return scale, sw, sh

if __name__ == "__main__":
    p = ArgumentParser("create an image pyramid")
    p.add_argument("input")
    p.add_argument("--tilesize", type=int, default=256, help="default: 256")
    p.add_argument("--output", default="./{base}.Z{z}.png", help="output template, default: ./{base}.Z{z}.png")
    args = p.parse_args()

    output = format_preprocess(args.output)
    d, f = os.path.split(args.input)
    base, ext = os.path.splitext(f)
    output = output.replace("{0[base]}", base)
    im = Image.open(args.input).convert("RGBA")
    scale, sw, sh = pyramid(im, output, args.tilesize)




