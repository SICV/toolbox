(function () {

/*
todo: reordering possibilities!
*/

function toa (x) {
	return Array.prototype.slice.apply(x);
}

function layers (control_elt, layer_elts) {
	var elt = control_elt,
		layer_id = 0;

	toa(layer_elts).forEach(function (imgelt) {
		// console.log("*", imgelt);
		var div = document.createElement("div"),
		 	input = document.createElement("input"),
		 	label = document.createElement("label"),
		 	lid = "aalayercntrl" + (++layer_id),
		 	name = imgelt.getAttribute("class"),
		 	static_layer = false;

		 elt.appendChild(div);
		 div.appendChild(input);
		 div.appendChild(label);

		 input.setAttribute("type", "checkbox");
		 input.setAttribute("id", lid);
		 label.setAttribute("for", lid);
		 label.innerHTML = name;
		 var s = getComputedStyle(imgelt);
		 var hidden = (s.display == "none");
		 input.checked = !hidden;

		 static_layer = (s.position == "static");

		 function hide () {
		 	if (static_layer) {
		 		imgelt.style.visibility = "hidden";
		 	} else {
		 		imgelt.style.display = "none";
		 	}
		 }
		 function show () {
		 	if (static_layer) {
		 		imgelt.style.visibility = "visible";
		 	} else {
		 		imgelt.style.display = "block";
		 	}
		 }

		 input.addEventListener("change", function () {
		 	this.checked ? show() : hide();
		 });
		 // console.log("s", s.display);
	});
}
window.layers = layers;

})();
