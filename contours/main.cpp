#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

using namespace cv;
using namespace std;

Mat src; Mat src_gray;
int thresh = 40;
int max_thresh = 305;
RNG rng(12345);

/// Function header
void thresh_callback(int, void* , int, char** argv);

/** @function main */
int main( int argc, char** argv )
{
    if (argc < 2) {
        fprintf(stderr, "usage: contours IMAGE [output.png]\n");
        return (0);
    }

    /// Load source image and convert it to gray
    src = imread( argv[1], 1 );

    /// Convert image to gray and blur it
    cvtColor( src, src_gray, CV_BGR2GRAY );
    blur( src_gray, src_gray, Size(3,3) );
    blur( src_gray, src_gray, Size(3,3) );
    blur( src_gray, src_gray, Size(3,3) );

    // char* source_window = "Source";
    /// Create Window
    //  namedWindow( source_window, CV_WINDOW_AUTOSIZE );
    //  imshow( source_window, src );

    thresh_callback( 0, 0, argc, argv );

    //  waitKey(0);
    return(0);
}

/** @function thresh_callback */
void thresh_callback(int, void* , int argc, char **argv)
{
    int totalpoints=0;
    Mat canny_output;
    Mat kerode, kdilate;
    vector<vector<Point> > contours;
    vector<Vec4i> hierarchy;
    dilate(src_gray,src_gray,kdilate);

    /// Detect edges using canny
    Canny( src_gray, canny_output, thresh, thresh*2, 3 );
    //  Laplacian(src_gray,canny_output,CV_8U,5);
    //  threshold(canny_output,canny_output,220,325,THRESH_BINARY_INV);
    /// Find contours
    findContours( canny_output, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );
    //  findContours( canny_output, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );
    /// Draw contours
    Mat drawing = Mat::zeros(canny_output.size(), CV_8UC3 );
    Mat cannyoutput = Mat::zeros( canny_output.size(), CV_8UC3 );
    int lastcontour=0;
    std::cout << "<svg version=\"1.1\" baseProfile=\"full\" width=\""<< drawing.cols <<"\" height=\""<< drawing.rows <<"\" xmlns=\"http://www.w3.org/2000/svg\">";
    for( int i = 0; i< contours.size(); i++ )
    {
        //contours avec nombre croissant de points
        //      if(contours[i].size()>lastcontour){
        //contours au-dessus de limite de points
        if(contours[i].size()>60){
            std::cout << "<polyline points=\"";
                  for(int j=0;j<contours[i].size();j++){
                      std::cout <<"  "<< contours[i][j].x << ","<< contours[i][j].y;
                  }

                  std::cout<< "\"   style=\"fill:none;stroke:rgb("<< rng.uniform(0,225) <<","<< rng.uniform(0,225)<<","<< rng.uniform(0,225)<<");stroke-width:3\" />\n";
            Scalar color = Scalar( rng.uniform(0, 225), rng.uniform(0,225), rng.uniform(0,225) );
            //Scalar color=Scalar(255,255,255);
            drawContours( drawing, contours, i, color, 1, CV_AA, hierarchy, 0, Point() );
            lastcontour=contours[i].size();
            totalpoints+=lastcontour;
//            std::cout << "number of points in last contour: " << lastcontour << "\n";
        }
    }
    std::cout << "</svg>\n";

    int totalcontours=contours.size();
    int ratio=totalpoints/totalcontours;
//    printf("%d\n",ratio);
    // std::cerr << ratio << "\n";
    fprintf(stderr, "%d\n", ratio);

    if (argc >= 3) {
        imwrite(argv[2],drawing);
    }
}
